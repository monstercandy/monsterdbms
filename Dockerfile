FROM monstercommon

ADD lib /opt/MonsterDbms/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterDbms/lib/bin/dbms-install.sh && /opt/MonsterDbms/lib/bin/dbms-test.sh

ENTRYPOINT ["/opt/MonsterDbms/lib/bin/dbms-start.sh"]
