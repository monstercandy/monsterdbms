module.exports = function(app, knex) {

    var router = app.ExpressPromiseRouter()

    const dotq = require("MonsterDotq");
    const dbmsLib = require("lib-dbms.js")(app, knex);

    router.get("/", function(req,res,next){
        return req.sendPromResultAsIs(
            dbmsLib.GetDbms()
            .then(dbmss=>{
               var re = {};
               return dotq.linearMap({array:dbmss, action: function(dbms){
                  return dbms.GetBackend()
                   .then(b=>{
                      return b.GetScoreboard()
                   })
                   .then(s=>{
                      re[dbms.dm_dbms_instance_name] = { scoreboard: s };
                   })
               }})
               .then(()=>{
                  return re;
               })               
            })            
          )
    })


  return router


}
