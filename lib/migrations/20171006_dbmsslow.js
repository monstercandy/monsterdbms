// NOTE: each entity here is related to the current server, since MonsterDbms'll always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.createTable('dbmsslow', function(table) {

            table.increments('ds_id').primary();

            table.uuid('ds_user_id');
            table.integer('ds_webhosting').notNullable();

            table.integer('ds_dbms')
                 .references('dm_id')
                 .inTable('dbms');

            table.string('ds_database_name').notNullable().defaultTo("");
            table.string('ds_db_username').notNullable();
            table.string('ds_query').notNullable();

            table.integer('ds_time');
            table.integer('ds_locktime');
            table.integer('ds_rows_sent');
            table.integer('ds_rows_examined');

            table.timestamp('ds_timestamp').notNullable().defaultTo(knex.fn.now());

        }),


    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('dbmsslow'),
    ])

};
