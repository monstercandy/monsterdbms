// NOTE: each entity here is related to the current server, since MonsterDbms'll always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.createTable('dbms', function(table) {
            table.increments('dm_id').primary();

            table.string('dm_dbms_instance_name').notNullable().unique();

            table.string('dm_dbms_type').notNullable(); // like mysql, postgres
            table.string('dm_connection_string').notNullable(); // connection string or json serialized parameters

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
        }),


         knex.schema.createTable('databases', function(table) {
            table.increments('db_id').primary();

            table.integer('db_dbms')
                 .references('dm_id')
                 .inTable('dbms');

            table.boolean('db_locked').notNullable().defaultTo(false);

            table.uuid('db_user_id');

            table.integer('db_webhosting').notNullable();

            table.string('db_database_name').notNullable();

            table.decimal('db_tally_mb').notNullable().defaultTo(0);            

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
         }),

         knex.schema.createTable('dbusers', function(table) {
            table.increments('u_id').primary();

            table.integer('u_dbms')
                 .references('dm_id')
                 .inTable('dbms');

            table.integer('u_database')
                 .references('db_id')
                 .inTable('databases');

            table.uuid('u_user_id');

            table.integer('u_webhosting').notNullable();

            table.string('u_username').notNullable();
            table.string('u_hostname').notNullable();

            table.boolean('u_locked').notNullable().defaultTo(false);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());

         }),

         knex.schema.createTable('webhostingstats', function(table) {
            table.increments('ws_id').primary();

            table.integer('ws_webhosting').notNullable();
            table.boolean('ws_locked').notNullable().defaultTo(false);
            table.integer('ws_database_count').notNullable().defaultTo(0);
            table.decimal('ws_tally_mb').notNullable().defaultTo(0);            

         })


    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('clusters'),
        knex.schema.dropTable('dbms'),
        knex.schema.dropTable('databases'),
        knex.schema.dropTable('dbusers'),
        knex.schema.dropTable('webhostingstats'),
    ])
	
};
