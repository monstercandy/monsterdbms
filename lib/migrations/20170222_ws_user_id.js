// NOTE: each entity here is related to the current server, since MonsterDbms'll always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.alterTable('webhostingstats', function(table) {
            table.string('ws_user_id').notNullable().defaultTo("");

        }),

    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostingstats', function(table){
            table.dropColumn("ws_user_id");
        }),        
    ])
	
};
