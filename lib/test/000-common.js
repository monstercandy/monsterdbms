module.exports = function(mapi, assert) {


  var re = {}

  re.mysql_connection_string = JSON.stringify({user:'root',password:'pw',host:'host'})



  re.mysql_slowlog_event = `
# Time: 171006 19:02:13
# User@Host: root[root] @ localhost []
# Thread_id: 55347731  Schema:   QC_hit: No
# Query_time: 10.000187  Lock_time: 0.000000  Rows_sent: 1  Rows_examined: 0
use mm_monstermedia;
SET timestamp=1507309333;
select sleep(10);
`;

  re.mysql_slowlog_event_unknown_database = `
# Time: 171006 19:02:13
# User@Host: root[root] @ localhost []
# Thread_id: 55347731  Schema:   QC_hit: No
# Query_time: 10.000187  Lock_time: 0.000000  Rows_sent: 1  Rows_examined: 0
use xxxxxxxxxxxxxxxx;
SET timestamp=1507309333;
select sleep(10);
`;

  re.setupDatabaseBackend=function(assert, DatabaseBackendLibStats, app){
    var oGetBackend = app.getDatabaseBackendLib;
    extend(DatabaseBackendLibStats, {
        QueryLiveStatistics: 0,
        Test: 0,
        CreateDatabase: 0,
        DeleteDatabase: 0,
        CreateUser: 0,
        DeleteUser: 0,
        GetPasswordForUsernames: 0,
    });
    app.getDatabaseBackendLib = function(dbmsType, connectionString) {
       assert.equal(dbmsType, "mysql")
       var re = {}
       Object.keys(DatabaseBackendLibStats).forEach(k=>{
          re[k] = function(p1){
             DatabaseBackendLibStats[k]++
             var x
             if(k == "QueryLiveStatistics")
                x = { information_schema: 0.00976563,
  dbcreated1: 0.06587029,
  dbcreated2: 25.90453053,
  mysql: 0.70346832}

             if(k == "GetPasswordForUsernames") {
                x = {};
                p1.forEach(user=>{
                   x[user] = "password-"+user;
                })
             }

             return Promise.resolve(x)
          }
       })
       return Promise.resolve(re)
    }
    return oGetBackend;
  }

  re.insertDbms = function(dbmsName) {
        it('inserting a dbms should work: '+dbmsName, function(done) {

             mapi.put( "/dbms/", {
                  "dm_dbms_instance_name": dbmsName,
                  "dm_dbms_type": "mysql",
                  "dm_connection_string": re.mysql_connection_string,
                },  function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

  }

  re.getValidDatabaseCreatePayload=function(databaseName, dbmsName, webhosting_id){
     return {
                  "db_dbms": dbmsName || "dbms-for-dbs",
                  "db_user_id": "123",
                  "db_server": "dbstest_withmysqlrole",
                  "db_webhosting": webhosting_id || "12345",
                  "db_database_name": databaseName
                }
  }

  re.insertDatabase= function(databaseName, dbmsName, webhosting_id){

        it('inserting a new database', function(done) {

             mapi.put( "/databases/", re.getValidDatabaseCreatePayload(databaseName, dbmsName, webhosting_id),
                function(err, result, httpResponse){
                    console.log("err", err, result)
                    assert.isNull(err)
                    assert.equal(result,"ok")
                    done()

             })
        })

  }

  re.setupMonsterInfoAccount = function(app){

        app.MonsterInfoAccount = {
            GetInfo: function(account_id){
                assert.equal(account_id, '123')
                return Promise.resolve({"u_id":123})
            }
        }

  }


  re.setupMonsterInfoWebhosting = function(app, webhosting_id){
        const expected_wh_id = webhosting_id || "12345";

        app.MonsterInfoWebhosting = {
            GetInfo: function(d){
                // console.log("GetInfo", d)
                assert.equal(""+d, ""+expected_wh_id)

                return Promise.resolve(
                {
                   wh_id: expected_wh_id,
                   wh_user_id: "123",
                   wh_is_expired: false,
                   wh_server: "dbstest_withmysqlrole",
                   wh_max_execution_second: 30,
                   wh_php_fpm_conf_extra_lines: "line1\nline2\n",
                   wh_name: "some name",
                   wh_template: "WEB10000",
                   wh_template_override: "{\"t_storage_max_quota_hard_mb\":20000}",
                   wh_php_version: "5.6",
                   template: {
                    t_db_max_number_of_users:2,
                    t_db_max_number_of_databases:2
                   },
                   extras: {
                     web_path: "/web/w3/"+d+"-12345678789"
                   }
                })
            }
        }

  }

  return re
}