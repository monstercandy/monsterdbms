require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../dbms-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var common = require("000-common.js")(mapi, assert)

    var oGetBackend = app.getDatabaseBackendLib
    var DatabaseBackendLibStats = {
        Import: 0,
        Export: 0,
        CreateDatabase: 0,
        DeleteDatabase: 0,
        CreateUser: 0,
        DeleteUser: 0,
        Repair: 0,
    }
    app.getDatabaseBackendLib = function(dbmsType, connectionString) {
       assert.equal(dbmsType, "mysql")
       return oGetBackend(dbmsType, connectionString)
         .then(backend=>{

               var re = {}
               Object.keys(DatabaseBackendLibStats).forEach(k=>{
                  re[k] = function(p1,p2,p3){
                     DatabaseBackendLibStats[k]++

                     if(Array("Export", "Import", "Repair").indexOf(k) > -1)
                        return backend[k](p1,p2,p3)

                     return Promise.resolve()
                  }
               })
               return Promise.resolve(re)


         })
    }


	describe('database related tests', function() {

        common.setupMonsterInfoAccount(app)
        common.setupMonsterInfoWebhosting(app)

        common.insertDbms("dbms-for-import-export")

        common.insertDatabase("dbimportexport", "dbms-for-import-export")



        it('exporting the database', function(done) {

             app.commander = {
                 spawn: function(task,params) {
                    // console.log("t",task, "p",params)
                    assert.property(task, "downloadAs")
                    delete task.downloadAs
                    assert.deepEqual(task, { chain:
   { executable: '/opt/MonsterDbms/lib/cmds/gzip-wrapper.sh',
     args:
      [ '[db_webhosting]',
        '[bin_mysqldump]',
        '-h',
        '[host]',
        '-u',
        '[user]',
        '--password=[password]',
        '[db_database_name]' ] },
  download: true,
  downloadMimeType: 'application/gzip' })

                    return Promise.resolve({id:"someid_export", executionPromise: Promise.resolve()})
                 }
             }

             mapi.post( "/databases/12345/dbimportexport/export", {},
                function(err, result, httpResponse){

                    assert.propertyVal(result, "id", "someid_export")
                    done()
             })
        })


        it('importing the database', function(done) {

             app.commander = {
                 spawn: function(task,params) {
                    // console.log("t",task, "p",params)
                    assert.property(params[2], "u_username")
                    assert.property(params[2], "u_password")

                    assert.deepEqual(task, { chain:
                           { executable: '/opt/MonsterDbms/lib/cmds/gunzip-wrapper.sh',
                             args:
                              [ '[db_webhosting]',
                                '[gunzip]',
                                '[bin_mysql]',
                                '-h',
                                '[host]',
                                '-u',
                                '[u_username]',
                                '--password=[u_password]',
                                '[db_database_name]' ] },
                         upload: true,
                    })

                    assert.propertyVal(params[4], "gunzip", "gunzip")
                    return Promise.resolve({id:"someid_import", executionPromise: Promise.resolve()})
                 }
             }

             mapi.post( "/databases/12345/dbimportexport/import", {filename:"anything.sql.gz"},
                function(err, result, httpResponse){
                    assert.propertyVal(result, "id", "someid_import")
                    done()
             })
        })

  })




  describe("repair", function(){


        it('repairing a database', function(done) {

            app.commander = {
                 spawn: function(task,params) {
                    // console.log("t",task, "p",params)
                    assert.deepEqual(task, { chain:
   { executable: '[bin_mysqlcheck]',
     args:
      [ '-h',
        '[host]',
        '-u',
        '[user]',
        '--password=[password]',
        '--repair',
        '--databases',
        '[db_database_name]',
        '--tables',
        'table_name' ] } })

                    return Promise.resolve({id:"someid_repair", executionPromise: Promise.resolve()})
                 }

            }

             mapi.post( "/databases/12345/dbimportexport/repair", {},
                function(err, result, httpResponse){
                    assert.isNull(err)
                    done()
             })
        })

  })


  describe("cleanup", function(){
        it('deleting the database', function(done) {

            app.commander = {
              EventEmitter: function() {
                  var emitter = {
                     send_stdout: function(msg){
                        console.log("received via stdout", msg)
                     },
                     send_stderr: function(msg){
                        console.log("received via stderr", msg)
                        assert.fail("we did not expect any errors")
                     },
                     close: function(x){
                        if(typeof x != "undefined") assert.fail("expected no error", x)
                        done()
                     },
                     spawn: function(x) {
                         console.log("via eventemitter", x)
                         if(typeof x != "undefined") assert.fail("expected no params", x)
                         return Promise.resolve({id:"anything"})
                     }
                  }

                  return emitter
              },
              spawn: function(c){
                 console.log("spawn:", c)
                 assert.fail("should not be here")
               }

            }

             mapi.delete( "/databases/12345/dbimportexport", {},
                function(err, result, httpResponse){
                    assert.isNull(err)
             })
        })

        it('deleting the dbms', function(done) {

             mapi.delete( "/dbms/dbms-for-import-export", {},
                function(err, result, httpResponse){
                    assert.isNull(err)
                    done()
             })
        })

  })






}, 10000)

