require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../dbms-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const fs = require("MonsterDotq").fs();

    var common = require("000-common.js")(mapi, assert);
    common.setupMonsterInfoAccount(app);
    common.setupMonsterInfoWebhosting(app);
    var DatabaseBackendLibStats={};
    var oGetBackend = common.setupDatabaseBackend(assert, DatabaseBackendLibStats, app);

	describe('inserting an event', function() {

        common.insertDbms("mysql-slow");

        common.insertDatabase("mm_monstermedia", "mysql-slow");

        it('list of slowlog events should be empty', function(done) {

             mapi.post( "/slowlog/all/search", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                 assert.property(result, "events")
                assert.ok(Array.isArray(result.events))
                assert.equal(result.events.length, 0);
                done()

             })

        })



        it('getting an event parsed', function() {

            const slowfn = app.config.get("watch_slowlog")["mysql-slow"];

            return fs.writeFileAsync(slowfn, "")
              .then(()=>{

                   return mapi.postAsync( "/slowlog/all/reopen", {})

              })
              .then(()=>{

                   return fs.writeFileAsync(slowfn, common.mysql_slowlog_event);

              })
              .then(()=>{
                  return new Promise((resolve)=>{
                     return setTimeout(resolve, 1000);
                  })
              })
              .then(()=>{
                  return mapi.postAsync( "/slowlog/all/search", {})

              })
              .then((re)=>{
                  // console.log(re.result.events);
                 assert.deepEqual(re.result.events, [ { ds_id: 1,
    ds_user_id: '123',
    ds_webhosting: 12345,
    ds_dbms: 'mysql-slow',
    ds_database_name: 'mm_monstermedia',
    ds_db_username: 'root',
    ds_query: 'select sleep(10);',
    ds_time: 10,
    ds_locktime: 0,
    ds_rows_sent: 1,
    ds_rows_examined: 0,
    ds_timestamp: '2017-10-06T17:02:13.000Z' } ]);
              })


        })



        it('events with unknown database name should not break stuff', function() {

            const slowfn = app.config.get("watch_slowlog")["mysql-slow"];

            return fs.appendFileAsync(slowfn, common.mysql_slowlog_event_unknown_database)
              .then(()=>{
                  return new Promise((resolve)=>{
                     return setTimeout(resolve, 1000);
                  })
              })
              .then(()=>{
                  return mapi.postAsync( "/slowlog/all/search", {})

              })
              .then((re)=>{
                 // console.log(re.result.events);
                 assert.deepEqual(re.result.events, [ { ds_id: 1,
    ds_user_id: '123',
    ds_webhosting: 12345,
    ds_dbms: 'mysql-slow',
    ds_database_name: 'mm_monstermedia',
    ds_db_username: 'root',
    ds_query: 'select sleep(10);',
    ds_time: 10,
    ds_locktime: 0,
    ds_rows_sent: 1,
    ds_rows_examined: 0,
    ds_timestamp: '2017-10-06T17:02:13.000Z' },
  { ds_id: 2,
    ds_user_id: '',
    ds_webhosting: 0,
    ds_dbms: 'mysql-slow',
    ds_database_name: 'xxxxxxxxxxxxxxxx',
    ds_db_username: 'root',
    ds_query: 'select sleep(10);',
    ds_time: 10,
    ds_locktime: 0,
    ds_rows_sent: 1,
    ds_rows_examined: 0,
    ds_timestamp: '2017-10-06T17:02:13.000Z' } ]);
              })


        })

	})




}, 10000)

