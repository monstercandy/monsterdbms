require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../dbms-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var common = require("000-common.js")(mapi, assert)

    const storage_id = 82345

    var latestBackup;
    var getInfoCalls = 0;
            app.MonsterInfoWebhosting = {
               GetInfo: function(id){


                   getInfoCalls++

                   return Promise.resolve({
                     wh_id: id,
                     wh_user_id: 123,
                     wh_storage: "/web/w3",
                     extras: {
                         web_path: "/web/w3/"+id+"-sdfsdfsdf"
                     },
                     template:{
                       t_ftp_max_number_of_accounts:1,
                       t_storage_max_quota_soft_mb:10,
                     },
                     wh_tally_web_storage_mb: 5.0,
                     wh_tally_sum_storage_mb: 5.2
                   })
               }
            }

    const expectedBackup = { databases:
   [ {
       props: {
          db_locked: 0,
          db_database_name: 'dbwie',
          db_tally_mb: 0,
          db_dbms: 'dbms-for-wie',
       },
       users:[
        {
             u_username: 'wieuser',
             u_hostname: '',
             u_locked: 0,
             u_password: 'password-wieuser'
        }
       ]
     }
   ]};

      const userData1 = {
          fa_webhosting:storage_id,fa_username:"wie-username",fa_password:"wie-password",fa_subdir:"/subdir"
      }



    var DatabaseBackendLibStats = {};
    var oGetBackend = common.setupDatabaseBackend(assert, DatabaseBackendLibStats, app);


	describe('preparation', function() {

        common.setupMonsterInfoAccount(app)

        common.insertDbms("dbms-for-wie")

        common.insertDatabase("dbwie", "dbms-for-wie", storage_id)

        it('inserting a database user', function(done) {
             mapi.put( "/databases/"+storage_id+"/dbwie/users", {u_username:"wieuser",u_password:"wiepasswd"},
                function(err, result, httpResponse){
                   assert.equal(result, "ok")
                   done()
                }
             )

        })

	})


  describe("backup", function(){

        backupShouldWork();

  })



  describe("restore", function(){

        cleanup();

        it('get list of databases should be empty', function(done) {

             mapi.get( "/databases/"+storage_id,function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })

        restoreShouldWork();

        // even twice, without clenaup!
        restoreShouldWork();

        backupShouldWork();

  });

  describe("site-wide backup", function(){

       it('generating backup for the complete site', function(done) {

             mapi.get( "/wie/", function(err, result, httpResponse){

                // console.log("foo", result)
                assert.ok(result[storage_id]);

                done()

             })

        })


       cleanup();

  })

  function restoreShouldWork(){
        it('restoring a specific webstore', function(done) {

             mapi.post( "/wie/"+storage_id, latestBackup, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()

             })

        })
  }


  function cleanup(){
      it("cleanup first", function(done){

           const emitterID = "foo";

            app.commander = {
              EventEmitter: function() {
                  var execRes, execRej
                  var execProm = new Promise(function(resolve,reject){execRes=resolve, execRej=reject})
                  var emitter = {
                     send_stdout: function(msg){
                     },
                     send_stderr: function(msg){
                     },
                     close: function(x){
                        if(typeof x != "undefined") assert.fail("expected no error", x)
                        execRes() // task execution is ready!

                        done()

                     },
                     spawn: function(x) {
                         console.log("via eventemitter", x)
                         if(typeof x != "undefined") assert.fail("expected no params", x)
                         return Promise.resolve({id:emitterID, emitter: emitter, executionPromise: execProm})
                     }
                  }

                  return emitter
              },
              spawn: function(c){
                 assert.fail("should not be here")
               }

            }

             mapi.delete( "/databases/"+storage_id, {}, function(err, result, httpResponse){

                assert.ok(result.id);

             })
      })
  }

  function backupShouldWork(){
       it('generating backup for a specific webstore', function(done) {

             mapi.get( "/wie/"+storage_id, function(err, result, httpResponse){

                // console.log("wie!", result)

                latestBackup = simpleCloneObject(result);

                var actual = result;

                actual.databases.forEach(db=>{
                   Array("created_at", "updated_at").forEach(x=>{
                      assert.ok(db.props[x]);
                      delete db.props[x];
                   })

                   db.users.forEach(user=>{
                       Array("created_at", "updated_at").forEach(x=>{
                          assert.ok(user[x]);
                          delete user[x];
                       })

                   })
                })

                assert.deepEqual(actual, expectedBackup);
                done()

             })

        })
  }


}, 10000)

