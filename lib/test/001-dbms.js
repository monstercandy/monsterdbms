require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../dbms-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var common = require("000-common.js")(mapi, assert)



	describe('basic tests', function() {



        shouldBeEmpty()

        it('unknown dbms should throw an error', function(done) {

             mapi.get( "/dbms/something",function(err, result, httpResponse){
                assert.propertyVal(err, "message", "DBMS_NOT_FOUND")
                done()

             })

        })


        it('trying to insert a dbms with invalid connection string', function(done) {

             mapi.put( "/dbms/", {
                  "dm_dbms_instance_name": "doesntmatter",
                  "dm_dbms_type": "mysql",
                  "dm_connection_string":JSON.stringify({hello:'there'}),
                },  function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.isNotNull(err)
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                assert.property(err, "errorParameters")
                assert.property(err.errorParameters, "host")
                assert.property(err.errorParameters, "user")
                // assert.property(err.errorParameters, "password")
                done()
             })

        })



        common.insertDbms("some-dbms")

        it('testing connection to the backend', function(done) {

            var backendCalled = 0
            var testCalled = 0
            var oBackend = app.getDatabaseBackendLib
            app.getDatabaseBackendLib = function(dbms_type, conn_str){
                assert.equal(dbms_type,"mysql")
                backendCalled++
                return Promise.resolve({
                  Test: function(){
                      testCalled++
                      return Promise.resolve("oky")
                  }
                })
            }

            mapi.post("/dbms/some-dbms/test", {}, function(err, result, httpResponse){
                    assert.equal(result, "oky")
                    assert.equal(backendCalled, 1)
                    assert.equal(testCalled, 1)
                    app.getDatabaseBackendLib = oBackend
                    done()

             })
        })


        it('fetching scoreboard of the backend', function(done) {

            const expectedResult = [{Id:15, User: "root", Host: "localhost", db:"some_db", Command: "SELECT * FROM nowhere", Time: 123, State: "Waiting for INSERT", Info: null, Progress: 0.00}]

            var backendCalled = 0
            var testCalled = 0
            var oBackend = app.getDatabaseBackendLib
            app.getDatabaseBackendLib = function(dbms_type, conn_str){
                assert.equal(dbms_type,"mysql")
                backendCalled++
                return Promise.resolve({
                  GetScoreboard: function(){
                      testCalled++
                      return Promise.resolve(expectedResult)
                  }
                })
            }

            mapi.get("/dbms/some-dbms/scoreboard",  function(err, result, httpResponse){
                    assert.deepEqual(result, expectedResult)
                    assert.equal(backendCalled, 1)
                    assert.equal(testCalled, 1)
                    app.getDatabaseBackendLib = oBackend
                    done()

             })
        })

        it('fetching scoreboard via the time machine', function(done) {

            const expectedResult = [{Id:15, User: "root", Host: "localhost", db:"some_db", Command: "SELECT * FROM nowhere", Time: 123, State: "Waiting for INSERT", Info: null, Progress: 0.00}]

            var backendCalled = 0
            var testCalled = 0
            var oBackend = app.getDatabaseBackendLib
            app.getDatabaseBackendLib = function(dbms_type, conn_str){
                assert.equal(dbms_type,"mysql")
                backendCalled++
                return Promise.resolve({
                  GetScoreboard: function(){
                      testCalled++
                      return Promise.resolve(expectedResult)
                  }
                })
            }

            mapi.get("/tm",  function(err, result, httpResponse){
                    assert.deepEqual(result, {"some-dbms": {scoreboard: expectedResult}})
                    assert.equal(backendCalled, 1)
                    assert.equal(testCalled, 1)
                    app.getDatabaseBackendLib = oBackend
                    done()
            })
        })

        it('inserting with duplicate name should be rejected', function(done) {

             mapi.put( "/dbms/", {
                  "dm_dbms_instance_name": "some-dbms",
                  "dm_dbms_type": "mysql",
                  "dm_connection_string": common.mysql_connection_string,
                },
                function(err, result, httpResponse){
                    assert.propertyVal(err, "message", "ALREADY_EXISTS")
                    done()

             })
        })

        it('the just insterted dbms should be found by name', function(done) {

             mapi.get( "/dbms/some-dbms",function(err, result, httpResponse){
                // console.log(result)
                assert.property(result, "created_at")
                delete result.created_at
                delete result.updated_at
                assert.propertyVal(result, "dm_dbms_instance_name", "some-dbms")
                assert.propertyVal(result, "dm_dbms_type", "mysql")
                done()

             })

        })


        it('and in the full list as well', function(done) {

             mapi.get( "/dbms",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 1)
                assert.propertyVal(result[0], "dm_dbms_instance_name", "some-dbms")
                done()

             })

        })


        it('delete should work', function(done) {

             mapi.delete( "/dbms/some-dbms",{},function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()

             })

        })

        shouldBeEmpty()


        it('inserting a dbms as part of a cluster', function(done) {

             mapi.put( "/dbms/", {
                  "dm_dbms_instance_name": "in_a_cluster",
                  "dm_dbms_type": "mysql",
                  "dm_connection_string": common.mysql_connection_string,
                },  function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        var new_conn_str = JSON.stringify({user:'foo',password:'bar',host:'hostx'})
        it('inserting a dbms as part of a cluster', function(done) {

             mapi.post( "/dbms/in_a_cluster", {
                  "dm_connection_string": new_conn_str,
                },  function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('the connection string should have changed', function(done) {

             mapi.get( "/dbms/in_a_cluster",function(err, result, httpResponse){
                // console.log(result)
                assert.propertyVal(result, "dm_connection_string", new_conn_str)
                done()

             })

        })


        it('list databases of a dbms', function(done) {

             mapi.get( "/dbms/in_a_cluster/databases",function(err, result, httpResponse){
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 0)
                done()

             })

        })


        it('delete should work', function(done) {

             mapi.delete( "/dbms/in_a_cluster",{},function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()

             })

        })

	})


  function shouldBeEmpty(){
        it('get dbms should be empty', function(done) {

             mapi.get( "/dbms",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })
  }


}, 10000)

