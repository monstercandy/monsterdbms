require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../dbms-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


    var common = require("000-common.js")(mapi, assert)

    const test1 = common.mysql_slowlog_event;

    const test2 = `
# Time: 171006 19:02:13
# User@Host: root[root] @ localhost []
# Thread_id: 55347731  Schema:   QC_hit: No
# Query_time: 10.000187  Lock_time: 0.000000  Rows_sent: 1  Rows_examined: 0
SET timestamp=1507309333;
select sleep(10);
`;

   const expected1 = {
  ds_db_username: 'root',
  ds_database_name: "mm_monstermedia",
  userhostline: '# User@Host: root[root] @ localhost []',
  ds_time: 10,
  ds_locktime: 0,
  ds_rows_sent: 1,
  ds_rows_examined: 0,
  ds_timestamp: '2017-10-06T17:02:13.000Z',
  ds_query: 'select sleep(10);' };

  const expected2 = { ds_db_username: 'root',
  userhostline: '# User@Host: root[root] @ localhost []',
  ds_time: 10,
  ds_locktime: 0,
  ds_rows_sent: 1,
  ds_rows_examined: 0,
  ds_timestamp: '2017-10-06T17:02:13.000Z',
  ds_query: 'select sleep(10);' }

     const parserLib = require("../lib-mysql-slowlog-parser.js")

     describe("parsing stuff", function(){

         it("supplying the stuff at once", function(){
            var eventCalls = 0;

            var parser = parserLib({
                onEvent: function(event){
                    //console.log(event);
                    assert.deepEqual(event, expected1);
                    eventCalls++;
                }
            })
            parser.processLine(test1);

            assert.equal(eventCalls, 1);

         })

         it("supplying the stuff line by line", function(){
            var eventCalls = 0;

            var parser = parserLib({
                onEvent: function(event){
                    //console.log(event);
                    assert.deepEqual(event, expected1);
                    eventCalls++;
                }
            })
            var testLines = test1.split("\n");
            testLines.forEach(line=>{
               parser.processLine(line);

            })

            assert.equal(eventCalls, 1);

         })


         it("when there is no database selection, it should still be able to parse it", function(){
            var eventCalls = 0;

            var parser = parserLib({
                onEvent: function(event){
                    // console.log(event);
                    assert.deepEqual(event, expected2);
                    eventCalls++;
                }
            })

            parser.processLine(test2);
            assert.equal(eventCalls, 1);


         })


     })


}, 10000)

