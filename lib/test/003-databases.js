require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../dbms-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var common = require("000-common.js")(mapi, assert)


    var DatabaseBackendLibStats = {};
    var oGetBackend = common.setupDatabaseBackend(assert, DatabaseBackendLibStats, app);


	describe('database related tests', function() {

        common.setupMonsterInfoAccount(app)

        common.insertDbms("dbms-for-dbs")
        common.insertDbms("dbms-on-other-for-dbs")

        databaseListShouldBeEmpty()

        it('query the dbms instances where creating a database is allowed', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.post( "/config/supported/databases/", {webhosting:"12345"},
                function(err, result, httpResponse){
                    assert.deepEqual(result, ["dbms-for-dbs", "dbms-on-other-for-dbs"])
                    done()
             })
        })

        it('same result should be received via webhosting route', function(done) {

             common.setupMonsterInfoWebhosting(app)

             mapi.post( "/databases/12345/supported", {},
                function(err, result, httpResponse){
                    assert.deepEqual(result, ["dbms-for-dbs", "dbms-on-other-for-dbs"])
                    done()
             })
        })


        common.insertDatabase("dbcreated1", "dbms-for-dbs")

        it("CreateDatabase on the backend should have been called", function(){
            assert.equal(DatabaseBackendLibStats.CreateDatabase, 1)
        })

        it('inserting a database with the same name again should fail', function(done) {

             mapi.put( "/databases/", common.getValidDatabaseCreatePayload("dbcreated1", "dbms-for-dbs"),
                function(err, result, httpResponse){
                    assert.propertyVal(err, "message", "ALREADY_EXISTS")
                    done()

             })
        })

        it('inserting a database with a slash should fail', function(done) {

             mapi.put( "/databases/", common.getValidDatabaseCreatePayload("dbcreated/foo", "dbms-for-dbs"),
                function(err, result, httpResponse){
                    assert.propertyVal(err, "message", "VALIDATION_ERROR")
                    done()

             })
        })


        it('inserting a new database via webhosting route this time', function(done) {

             mapi.put( "/databases/12345/", {
                  "db_dbms": "dbms-on-other-for-dbs",
                  "db_user_id": "123",
                  "db_database_name": "dbcreated2"
                },
                function(err, result, httpResponse){
                    // console.log("err", err, result)
                    assert.isNull(err)
                    assert.equal(result,"ok")
                    assert.equal(DatabaseBackendLibStats.CreateDatabase, 2) // one more since the previous
                    done()

             })
        })


        it('global limit is reached, empty list should be returned', function(done) {

             mapi.post( "/config/supported/databases/", {webhosting:"12345"},
                function(err, result, httpResponse){
                    assert.deepEqual(result, [])
                    done()

             })
        })


        it('inserting any database, should just fail', function(done) {

             mapi.put( "/databases/", common.getValidDatabaseCreatePayload("dbcreated3", "dbms-for-dbs"),
                function(err, result, httpResponse){
                    assert.propertyVal(err, "message", "SPECIFIED_DBMS_NOT_ALLOWED_OR_QUOTA_EXHAUSTED")
                    done()

             })
        })

        Array("/databases", "/databases/12345").forEach(path=>{

            it('should list the just inserted dbs, path: '+path, function(done) {

                 mapi.get( path,function(err, result, httpResponse){
                    // console.log("here",err, result)
                    for(var i = 0; i < result.length; i++){
                       delete result[i].created_at
                       delete result[i].updated_at
                    }
                    assert.deepEqual(result, [ {
        db_user_id: '123',
        db_locked: 0,
        db_webhosting: 12345,
        db_database_name: 'dbcreated1',
        dm_dbms_instance_name: "dbms-for-dbs",
        dm_dbms_type: "mysql",
        db_tally_mb: 0},
      {
        db_user_id: '123',
        db_locked: 0,
        db_webhosting: 12345,
        db_database_name: 'dbcreated2',
        dm_dbms_instance_name: "dbms-on-other-for-dbs",
        dm_dbms_type: "mysql",
        db_tally_mb: 0}
         ])
                    done()

                 })

            })

        })


        it('should be able to list all databases grouped by webhosting', function(done) {

             mapi.search( "/databases",function(err, result, httpResponse){
                // console.log(result)
                assert.property(result, "12345")
                assert.isTrue(Array.isArray(result[12345]))
                for(var i = 0; i < result[12345].length; i++){
                    delete result[12345][i].created_at
                    delete result[12345][i].updated_at
                }
                assert.deepEqual(result, {12345: [ {
    db_user_id: '123',
    db_locked: 0,
    db_webhosting: 12345,
    db_database_name: 'dbcreated1',
    dm_dbms_instance_name: "dbms-for-dbs",
    dm_dbms_type: "mysql",
    db_tally_mb: 0},
  {
    db_user_id: '123',
    db_locked: 0,
    db_webhosting: 12345,
    db_database_name: 'dbcreated2',
    dm_dbms_instance_name: "dbms-on-other-for-dbs",
    dm_dbms_type: "mysql",
    db_tally_mb: 0}
     ]})
                done()

             })

        })


        it('also by name', function(done) {

             mapi.get( "/dbms/dbms-for-dbs/database/dbcreated1",function(err, result, httpResponse){
                // console.log("here",err, result)
                delete result.created_at
                delete result.updated_at
                assert.deepEqual(result, {
    db_user_id: '123',
    db_locked: 0,
    db_webhosting: 12345,
    db_database_name: 'dbcreated1',
    dm_dbms_instance_name: "dbms-for-dbs",
    dm_dbms_type: "mysql",
    db_tally_mb: 0})
                done()

             })

        })


        it('also via a webhosting route', function(done) {

             mapi.get( "/databases/12345/dbcreated1",function(err, result, httpResponse){
                // console.log("here",err, result)
                delete result.created_at
                delete result.updated_at
                assert.deepEqual(result, {
    db_user_id: '123',
    db_locked: 0,
    db_webhosting: 12345,
    db_database_name: 'dbcreated1',
    dm_dbms_instance_name: "dbms-for-dbs",
    dm_dbms_type: "mysql",
    db_tally_mb: 0})
                done()

             })

        })


        it('but not by invalid name', function(done) {

             mapi.get( "/dbms/dbms-for-dbs/database/dbcreatedxx",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "DATABASE_NOT_FOUND")
                done()

             })

        })

        it('filtering databases based on dbms: dbms-for-dbs', function(done) {

             mapi.get( "/dbms/dbms-for-dbs/databases",function(err, result, httpResponse){
                // console.log("here",err, result)

                for(var i = 0; i < result.length; i++){
                   delete result[i].created_at
                   delete result[i].updated_at
                }

                assert.deepEqual(result, [ { db_user_id: '123',
    db_locked: 0,
    db_webhosting: 12345,
    db_database_name: 'dbcreated1',
    db_tally_mb: 0,
    dm_dbms_type: "mysql",
    dm_dbms_instance_name: 'dbms-for-dbs' }])
                done()

             })

        })

        it('filtering databases based on dbms: dbms-for-dbs', function(done) {

             mapi.get( "/dbms/dbms-on-other-for-dbs/databases",function(err, result, httpResponse){

                for(var i = 0; i < result.length; i++){
                   delete result[i].created_at
                   delete result[i].updated_at
                }

                // console.log("here",err, result)
                assert.deepEqual(result, [ { db_user_id: '123',
    db_locked: 0,
    db_webhosting: 12345,
    db_database_name: 'dbcreated2',
    db_tally_mb: 0,
    dm_dbms_type: "mysql",
    dm_dbms_instance_name: 'dbms-on-other-for-dbs' } ])
                done()

             })

        })

        it('deleting a db via webhosting route', function(done) {
            const emitterID= "emitterID"

            app.commander = {
              EventEmitter: function() {
                  var emitter = {
                     send_stdout: function(msg){
                        console.log("received via stdout", msg)
                     },
                     send_stderr: function(msg){
                        console.log("received via stderr", msg)
                        assert.fail("we did not expect any errors")
                     },
                     close: function(x){
                        if(typeof x != "undefined") assert.fail("expected no error", x)

                        assert.equal(DatabaseBackendLibStats.DeleteDatabase, 1)
                        done()

                     },
                     spawn: function(x) {
                         console.log("via eventemitter", x)
                         if(typeof x != "undefined") assert.fail("expected no params", x)
                         return Promise.resolve({id:emitterID, emitter: emitter})
                     }
                  }

                  return emitter
              },
              spawn: function(c){
                 console.log("spawn:", c)
                 assert.fail("should not be here")
               }

            }

             mapi.delete( "/databases/12345/dbcreated1",{},function(err, result, httpResponse){
                assert.propertyVal(result, "id", emitterID)

             })

        })

	})


    describe('testing database relocation', function() {

        var origAccountInfo;
        var origWebhostingInfo;
        var expectedUserId = "123";

        it("setting up account lookup helper", function(){
            origAccountInfo = app.MonsterInfoAccount;
            app.MonsterInfoAccount = {
                GetInfo: function(account_id){
                    return Promise.resolve({"u_id":account_id})
                }
            }

            origWebhostingInfo = app.MonsterInfoWebhosting;
            app.MonsterInfoWebhosting = {
                GetInfo: function(q_wh_id){
                    // console.log("GetInfo", d)
                    // assert.equal(""+d, ""+expected_wh_id)

                    return Promise.resolve(
                    {
                       wh_id: q_wh_id,
                       wh_user_id: expectedUserId,
                       wh_is_expired: false,
                       wh_server: "dbstest_withmysqlrole",
                       wh_max_execution_second: 30,
                       wh_php_fpm_conf_extra_lines: "line1\nline2\n",
                       wh_name: "some name",
                       wh_template: "WEB10000",
                       wh_template_override: "{\"t_storage_max_quota_hard_mb\":20000}",
                       wh_php_version: "5.6",
                       template: {
                        t_db_max_number_of_users:2,
                        t_db_max_number_of_databases:2
                       },
                       extras: {
                         web_path: "/web/w3/"+q_wh_id+"-12345678789"
                       }
                    })
                }
            }

        })

        common.insertDatabase("dbmove", "dbms-for-dbs")

        it('inserting a database user', function(done) {
            var before = DatabaseBackendLibStats.CreateUser
             mapi.put( "/databases/12345/dbmove/users",{u_username:"dbmoveuser",u_password:"fsdfsd"},
              function(err, result, httpResponse){
                assert.equal(result, "ok")
                assert.equal(DatabaseBackendLibStats.CreateUser, before+1)
                done()

             })

        })

        it("moving stuff", function(done){
            expectedUserId = 124;
            mapi.post("/databases/12345/dbmove/move", {dest_wh_id:12346,dest_u_id: 124}, function(err,result){
                assert.equal(result,"ok")
                done()
            })
        })

        it("querying database", function(done){

             mapi.get( "/databases/12346/dbmove",function(err, result, httpResponse){
                // console.log("here",err, result)
                delete result.created_at
                delete result.updated_at
                assert.deepEqual(result, {
    db_user_id: '124',
    db_locked: 0,
    db_webhosting: 12346,
    db_database_name: 'dbmove',
    dm_dbms_instance_name: "dbms-for-dbs",
    dm_dbms_type: "mysql",
    db_tally_mb: 0})
                done()

             })

        })

        it("querying user", function(done){

             mapi.get( "/databases/12346/dbmove/user/dbmoveuser",
              function(err, result, httpResponse){
                delete result.created_at
                delete result.updated_at
                assert.deepEqual(result, {
  u_webhosting: 12346,
  u_username: 'dbmoveuser',
  u_hostname: '',
  u_locked: 0,
  db_database_name: 'dbmove',
  dm_dbms_instance_name: 'dbms-for-dbs' })


                done()

             })

        })


        it('deleting a database user', function(done) {

             mapi.delete( "/databases/12346/dbmove/user/dbmoveuser",{},
              function(err, result, httpResponse){
                // console.log(err, result)
                assert.equal(DatabaseBackendLibStats.DeleteUser, 1)
                done()

             })

        })


        it('deleting a db via webhosting route', function(done) {
            const emitterID= "emitterID"

            app.commander = {
              EventEmitter: function() {
                  var emitter = {
                     send_stdout: function(msg){
                        console.log("received via stdout", msg)
                     },
                     send_stderr: function(msg){
                        console.log("received via stderr", msg)
                        assert.fail("we did not expect any errors")
                     },
                     close: function(x){
                        if(typeof x != "undefined") assert.fail("expected no error", x)

                        assert.equal(DatabaseBackendLibStats.DeleteDatabase, 2)

                        app.MonsterInfoAccount = origAccountInfo;
                        app.MonsterInfoWebhosting = origWebhostingInfo;

                        done()

                     },
                     spawn: function(x) {
                         console.log("via eventemitter", x)
                         if(typeof x != "undefined") assert.fail("expected no params", x)
                         return Promise.resolve({id:emitterID, emitter: emitter})
                     }
                  }

                  return emitter
              },
              spawn: function(c){
                 console.log("spawn:", c)
                 assert.fail("should not be here")
               }

            }

             mapi.delete( "/databases/12346/dbmove",{},function(err, result, httpResponse){
                assert.propertyVal(result, "id", emitterID)

             })

        })

   })

  describe('dbuser related tests', function() {

       dbuserListShouldBeEmptyViaDbmsRoute()

       dbuserListShouldBeEmptyViaWebhostingRoute()

       createDbUser()

        it('second with the same name should throw', function(done) {

             mapi.put( "/databases/12345/dbcreated2/users",{u_username:"foobar",u_password:"fsdfsd"},
              function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ALREADY_EXISTS")
                done()

             })

        })


        it('get a dbuser by name', function(done) {

             mapi.get( "/databases/12345/dbcreated2/user/foobar",
              function(err, result, httpResponse){
                delete result.created_at
                delete result.updated_at
                assert.deepEqual(result, {
  u_webhosting: 12345,
  u_username: 'foobar',
  u_hostname: '',
  u_locked: 0,
  db_database_name: 'dbcreated2',
  dm_dbms_instance_name: 'dbms-on-other-for-dbs' })
                done()

             })

        })

        it('deleting a database user', function(done) {

             mapi.delete( "/databases/12345/dbcreated2/user/foobar",{},
              function(err, result, httpResponse){
                // console.log(err, result)
                assert.equal(DatabaseBackendLibStats.DeleteUser, 2)
                done()

             })

        })

       createDbUser() // and insert should work again

  })

  describe('other', function() {


        it('query backup dirs', function(done) {

             mapi.get( "/databases/backupdirs",function(err, result, httpResponse){
                // console.log("!!!", err, result)
                assert.deepEqual(result, { '12345':
   { databases: [ 'dbcreated2' ],
     backupdir: '/web/w3/12345-12345678789/dbbackup' } })
                done()

             })

        })

        it('query aggregated database stats', function(done) {

             mapi.get( "/databases/stats",function(err, result, httpResponse){
                // console.log("!!!", err, result)
                assert.deepEqual(result, [ {
    db_user_id: "123",
    db_webhosting: 12345,
    c: 1,
    s: 0 } ])
                done()

             })

        })

        it('query aggregated and detailed database stats', function(done) {

             mapi.get( "/databases/stats-detailed",function(err, result, httpResponse){
                // console.log("!!!", err, result)
                assert.deepEqual(result, { forWebhosting: { '12345': { wh_tally_db_storage: { dbcreated2: 0 } }},
  aggregatedStats: [ { c: 1, s: 0, db_webhosting: 12345, db_user_id: '123' } ] })
                done()

             })

        })

        it('query webhosting based live stats', function(done) {

             mapi.get( "/stats/live",function(err, result, httpResponse){
                // console.log("!!!", err, result)
                assert.property(result, 'dbms-for-dbs');
                assert.property(result, 'dbms-on-other-for-dbs');
                assert.equal(DatabaseBackendLibStats.QueryLiveStatistics, 2)

                done()

             })

        })

        it("query global scoreboard", function(done){

            const dbmsResult = [{Id:15, User: "root", Host: "localhost", db:"some_db", Command: "SELECT * FROM nowhere", Time: 123, State: "Waiting for INSERT", Info: null, Progress: 0.00}]

            var backendCalled = 0
            var testCalled = 0
            var oBackend = app.getDatabaseBackendLib
            app.getDatabaseBackendLib = function(dbms_type, conn_str){
                assert.equal(dbms_type,"mysql")
                backendCalled++
                return Promise.resolve({
                  GetScoreboard: function(){
                      testCalled++
                      return Promise.resolve(simpleCloneObject(dbmsResult))
                  }
                })
            }

            mapi.get("/stats/scoreboard",  function(err, result, httpResponse){
                console.log("shit", result, result.length)
                    assert.ok(backendCalled)
                    assert.ok(testCalled)
                    assert.ok(Array.isArray(result))
                    result.forEach(line=>{
                       assert.property(line, "dm_dbms_instance_name")
                       delete line.dm_dbms_instance_name
                       assert.deepEqual(line, dbmsResult[0])
                    })

                    app.getDatabaseBackendLib = oBackend
                    done()

             })

        })


  })


  describe('cleanup', function() {

        it('deleting a webhosting should delete everything', function(done) {

            const emitterID= "emitterID2"

            app.commander = {
              EventEmitter: function() {
                  var execRes, execRej
                  var execProm = new Promise(function(resolve,reject){execRes=resolve, execRej=reject})
                  var emitter = {
                     send_stdout: function(msg){
                        console.log("received via stdout", msg)
                     },
                     send_stderr: function(msg){
                        console.log("received via stderr", msg)
                        assert.fail("we did not expect any errors")
                     },
                     close: function(x){
                        if(typeof x != "undefined") assert.fail("expected no error", x)
                        execRes() // task execution is ready!

                        assert.equal(DatabaseBackendLibStats.DeleteDatabase, 3)
                        done()

                     },
                     spawn: function(x) {
                         console.log("via eventemitter", x)
                         if(typeof x != "undefined") assert.fail("expected no params", x)
                         return Promise.resolve({id:emitterID, emitter: emitter, executionPromise: execProm})
                     }
                  }

                  return emitter
              },
              spawn: function(c){
                 console.log("spawn:", c)
                 assert.fail("should not be here")
               }

            }

             mapi.delete( "/databases/12345",{},function(err, result, httpResponse){
                assert.propertyVal(result, "id", emitterID)

             })

        })

        databaseListShouldBeEmpty()

  })


  function createDbUser(username){
        it('inserting a database user', function(done) {
            var before = DatabaseBackendLibStats.CreateUser
             mapi.put( "/databases/12345/dbcreated2/users",{u_username:username||"foobar",u_password:"fsdfsd"},
              function(err, result, httpResponse){
                assert.equal(result, "ok")
                assert.equal(DatabaseBackendLibStats.CreateUser, before+1)
                done()

             })

        })

  }


  function databaseListShouldBeEmpty(){
        it('get databases should be empty', function(done) {

             mapi.get( "/dbms/dbms-for-dbs/databases",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })
  }

  function dbuserListShouldBeEmptyViaDbmsRoute(){
        it('get db users should be empty via dbms route', function(done) {

             mapi.get( "/dbms/dbms-on-other-for-dbs/database/dbcreated2/users",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })
  }

  function dbuserListShouldBeEmptyViaWebhostingRoute(){
        it('get db users should be empty via webhosting route', function(done) {

             mapi.get( "/databases/12345/dbcreated2/users",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })
  }


}, 10000)

