(function(){

/*
// moment(Number);
this is what we need to parse:

# Time: 171006 19:02:13
# User@Host: root[root] @ localhost []
# Thread_id: 55347731  Schema:   QC_hit: No
# Query_time: 10.000187  Lock_time: 0.000000  Rows_sent: 1  Rows_examined: 0
SET timestamp=1507309333;
select sleep(10);
*/

    const Tail = require("MonsterTail").Tail;
    const moment = require("MonsterMoment");

    const databaseSelection = /^use (.+);/;
    const userHost = /^# User@Host: (.+)\[.+\] @ (.+)/;
    const queryTime = /^# Query_time: ([0-9\.]+)\s*Lock_time: ([0-9\.]+)\s*Rows_sent: ([0-9]+)\s*Rows_examined: ([0-9]+)/;
    const timeStamp = /^SET timestamp=([0-9]+)/;

    var lib = module.exports = function(options) {

       if(!options.onEvent) throw new Error("onEvent not provided");

       var re = {};

       var tail;

       var latestEvent = {};

       re.unwatch = function(){
         if(!tail) return;
         tail.unwatch();
         tail = null;
       }

       re.processLine = function(linesStr) {
          var lines = linesStr.split("\n");
          lines.forEach(line=>{
              if(!line) return;
              if(line.match(/^.*mysqld, Version:.+ started with:/)) return;
              if(line.match(/^Tcp port: \d+/)) return;
              if(line.match(/^^Time\s+Id\s+Command\s+Argument/)) return;

              var m = userHost.exec(line);
              if(m) {
                 latestEvent.ds_db_username = m[1];
                 latestEvent.userhostline = line;
                 return;
              }

              m = queryTime.exec(line);
              if(m) {
                 latestEvent.ds_time = parseInt(m[1], 10);
                 latestEvent.ds_locktime = parseInt(m[2], 10);
                 latestEvent.ds_rows_sent = parseInt(m[3], 10);
                 latestEvent.ds_rows_examined = parseInt(m[4], 10);
                 return;
              }

              m = timeStamp.exec(line);
              if(m) {
                 latestEvent.ds_timestamp = new moment(m[1]*1000).toISOString();
                 return;
              }

              m = databaseSelection.exec(line);
              if(m) {
                 latestEvent.ds_database_name = m[1];
                 return;
              }

              if(line.charAt(0) == "#") return;

              // at this point this must be the query itself.

              if(line.indexOf(";") < 0) return;

              latestEvent.ds_query = line;
              dispatchEvent();

          })
       }


       re.setupTail = function(){
          if(!options.filename) return;

          re.unwatch();

          tail = Tail.robust(options.filename);
          tail.on("line", re.processLine);
       }

       re.setupTail();

       return re;


       function dispatchEvent(){
          options.onEvent(latestEvent);
          latestEvent = {};
       }


    }

})()
