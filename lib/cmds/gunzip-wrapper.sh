#!/bin/bash

set -e

USER_ID="$1"
shift

USE_GZIP_WRAPPER="$1"
shift



if [ "$USE_GZIP_WRAPPER" == "gunzip" ]; then
  echo "Importing gzip compressed sql dump"
  gunzip | $@
else
  echo "Importing uncompressed sql dump"
  $@
fi

#chroot "--userspec=$USER_ID:65531" / /bin/bash <<EOF
#EOF
