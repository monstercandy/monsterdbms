var lib = module.exports = function(app, knex, database){

    const KnexLib = require("MonsterKnex")

    const MError = require("MonsterError")

    var ValidatorsLib = require("MonsterValidators")

  	var vali = ValidatorsLib.ValidateJs()


    var Common = ValidatorsLib.array()

	var re = {}

  function getUsersBy(filterHash) {

    // this is just a common filter:
    filterHash.u_database = database.db_id

    var f = KnexLib.filterHash(filterHash)

    return knex("dbusers").select()
      .leftJoin("databases","databases.db_id","dbusers.u_database")
      .leftJoin("dbms","dbms.dm_id","databases.db_dbms")
      .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
      .then(rows=>{
              rows.map(row => dbRowToObject(row))
              Common.AddCleanupSupportForArray(rows)

              return Promise.resolve(rows)
      })
  }


	re.GetUsers = function(){
		 return getUsersBy({})
	}
  re.GetUsersOfWebhosting = function(wh_id){
    console.log("getting users ", wh_id)
     return getUsersBy({u_webhosting:wh_id})
  }

  function shouldBeOneRow(prom) {
     return prom.then(rows=>{
           if(rows.length != 1) throw new MError("DBUSER_NOT_FOUND")

           return Promise.resolve(rows[0])
     })
  }

  re.GetUserByName = function(username){
     return shouldBeOneRow(getUsersBy({u_username: username}))
  }

  function InsertAllowed(in_data, force) {
      if(force) return Promise.resolve()

      var maxDbUsers
      return app.MonsterInfoWebhosting.GetInfo(in_data.u_webhosting)
        .then(webhosting=>{
            maxDbUsers = webhosting.template.t_db_max_number_of_users

            return re.GetUsers()
        })
        .then(users=>{
            if(users.length >= maxDbUsers) throw new MError("DATABASE_USER_LIMIT_IS_REACHED")

            return Promise.resolve()

        })

  }


	re.Insert = function(in_data, req){

      var password
      var id
      var d
      var dbuserData

      if(database.db_locked) return Promise.reject(new MError("DATABASE_IS_LOCKED"))

      if(!in_data.u_hostname)
        delete in_data.u_hostname;

      return vali.async(in_data, {
        "u_username": app.getDbnameConstraints(),
        "u_password": ValidatorsLib.passwordEnforcements,
        "u_hostname": {isString: true, isHost: true, default: {value:{d_host_canonical_name:""}}},
      })
    	.then(ad=>{

          d = ad

          d.u_hostname = d.u_hostname.d_host_canonical_name
          d.u_database = database.db_id
          d.u_dbms = database.db_dbms
          d.u_user_id = database.db_user_id
          d.u_webhosting = database.db_webhosting

          password = d.u_password
          delete d.u_password

          return re.GetUserByName(d.u_username)
            .then((user)=>{
               throw new MError("ALREADY_EXISTS", null, {conflictingUser: user})
            })
            .catch(ex=>{
               if(ex.message == "DBUSER_NOT_FOUND")
                  return Promise.resolve()
               throw ex
            })
      })
       .then(()=>{

          return InsertAllowed(d, in_data.force)
      })
       .then(()=>{

           return knex
             .insert(d)
             .into("dbusers")
             .then(ids=>{
                return Promise.resolve(ids[0])
             })
             .catch(ex=>{
                console.error("error creating row", ex)
                if(ex.code == 'SQLITE_CONSTRAINT')
                   throw new MError("ALREADY_EXISTS")
                throw ex
             })

        })
        .then((aid)=>{
           id = aid
           app.InsertEvent(req, {e_event_type: "dbuser-create",
		      e_user_id: d.u_user_id,
              e_other: {u_id:id, u_username: in_data.u_username}}
           )

           if(in_data.skip_backend) return Promise.resolve()

           return database.GetBackend()
             .then(backend=>{
                return backend.CreateUser(database.db_database_name, d.u_username, password, d.u_hostname)
             })
             .catch(ex=>{
                 console.error("error creating username:", ex)
                 if(in_data.force) return Promise.resolve()

                 throw ex
             })

        })
        .then(()=>{

           return Promise.resolve(id)
        })

	}

	return re


    function dbRowToObject(dbuser){

    	dbuser.Cleanup = function(){
    		delete dbuser.u_id
        delete dbuser.u_dbms
        delete dbuser.u_database
        delete dbuser.u_user_id

        Object.keys(dbuser).forEach(k=>{
           if((!k.match(/^(dm|db)_/))||(k.match(/^(dm_dbms_instance_name|db_database_name)$/))) return
           delete dbuser[k]
        })
    		return dbuser
    	}

      Array("ToReadOnly","ToReadWrite").forEach(c=>{
          dbuser[c] = function(req){
              return database.GetBackend()
                .then(backend=>{
                   return backend["User"+c](database.db_database_name,dbuser.u_username, dbuser.u_hostname)
                })
                .then(()=>{

                   app.InsertEvent(req, {
					   e_event_type: "dbuser-"+c,
		               e_user_id: dbuser.u_user_id,
					   e_other: {db_database_name:database.db_database_name, u_username: dbuser.u_username}
				   })

                   var mailer = app.GetMailer()

                   var x = {}
                   x.toAccount = {user_id: dbuser.u_user_id, emailCategory: "TECH"}
                   x.template = "dbms/"+c.toLowerCase()
                   x.context = {database: {db_database_name:database.db_database_name}, dbuser: {u_username:dbuser.u_username}, event: c }

                   mailer.SendMailAsync(x)


                  return knex('dbusers')
                        .whereRaw('u_id=?', [dbuser.u_id])
                        .update({u_locked: (c == "ToReadOnly")})
                })
                .then(()=>{
                  return Promise.resolve()
                })
          }

      })

    	dbuser.Delete = function(in_data, emitter, req){

              in_data = in_data || {}

  	          return knex("dbusers")
  	           .whereRaw("u_id=?", [dbuser.u_id])
  	           .del()
              .then(()=>{
                 app.InsertEvent(req, {
				    e_event_type: "dbuser-remove",
				    e_user_id: dbuser.u_user_id,
					e_other: {db_database_name:database.db_database_name}
			     })
              })
               .then(()=>{

                  if(in_data.skip_backend) return Promise.resolve()

                  return database.GetBackend()
                    .then(backend=>{
                       return backend.DeleteUser(dbuser.u_username, dbuser.u_hostname)
                    })
                    .catch(ex=>{
                        console.error("error deleting database", ex)
                        if(in_data.force) return Promise.resolve()
                        throw ex
                    })

               })
    	}


    	return dbuser


    }


}
