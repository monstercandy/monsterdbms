var lib = module.exports = function(app, knex){

    const KnexLib = require("MonsterKnex")

    const MError = require("MonsterError")

    var ValidatorsLib = require("MonsterValidators")

  	var vali = ValidatorsLib.ValidateJs()


    vali.validators.lookupDbms = function(value, options, key, attributes, glob) {
         if(typeof value != "string") return "invalid dbms name"

         var dbms = options.getDbmsLibInstance()
         return dbms.GetDbmsByName(value)
           .then(dbmsData=>{
               attributes.db_dbms = dbmsData
               return Promise.resolve()
           })
           .catch(ex=>{
              console.error("dbms lookup failed", ex)
              return Promise.resolve("dbms not found")
           })


      }

    var Common = ValidatorsLib.array()

	var re = {}

  function getDatabasesBy(filterHash) {

    var f = KnexLib.filterHash(filterHash)

    return knex("databases").select().leftJoin("dbms","dbms.dm_id","databases.db_dbms")
      .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
      .then(rows=>{

              rows.map(row => dbRowToObject(row))
              Common.AddCleanupSupportForArray(rows)

              return Promise.resolve(rows)
      })
  }

  re.StoreStatistics = function(dbmsId, databaseName, consumptionMb) {

      return knex('databases')
        .whereRaw('db_dbms=? AND db_database_name=?', [dbmsId,databaseName])
        .update({db_tally_mb: consumptionMb})
        .then(()=>{
           return Promise.resolve() // good!
        })

  }

  re.LockUnlockDatabasesBasedOnExceeding = function(webhosting, exceeded) {

     return re.GetDatabasesByWebhosting(webhosting)
       .then(dbs=>{
          var ps = []
          dbs.forEach(db=>{
              ps.push(db.GrantRevokeAllUsers(exceeded))
          })

          return Promise.all(ps)
       })
  }

  re.QueryBackupDirs = function(){
      var re = {}
      return knex("databases").select()
        .then(rows=>{
           rows.forEach(row=>{
              if(!re[row.db_webhosting]) re[row.db_webhosting] = {databases:[]}
              re[row.db_webhosting].databases.push(row.db_database_name)
           })

           var ps = []
           Object.keys(re).forEach(webhosting_id=>{
              ps.push(
                app.MonsterInfoWebhosting.GetInfo(webhosting_id)
                  .then(info=>{
                     const path = require("path")
                     re[webhosting_id].backupdir = path.posix.join(info.extras.web_path, "dbbackup")

                  })
                  .catch(ex=>{
                     console.error("Unable to query webhosting info for a database!", webhosting_id, ex)
                  })
              )
           })

           return Promise.all(ps)
        })
        .then(()=>{
           return Promise.resolve(re)
        })


  }

  function setupStatsFilter(knexO, onlyWebhosting) {
      var whereStr = "1"
      var whereFilter = []
      if(onlyWebhosting) {
         whereStr = "db_webhosting=?"
         whereFilter = [onlyWebhosting]
      }

      return knexO.whereRaw(whereStr, whereFilter)

  }

  re.QueryAggregatedStats = function(onlyWebhosting){

      return setupStatsFilter(knex("databases").select('db_webhosting','db_user_id').count("db_id AS c").sum("db_tally_mb AS s"), onlyWebhosting).groupBy('db_webhosting')
        .then(rows=>{
           return Promise.resolve(rows)
        })
  }



  re.QueryAggregatedStatsWithDetails = function(onlyWebhosting){


      var forWebhosting = {}
      var byWebhosting = {}
      return setupStatsFilter(knex("databases").select(), onlyWebhosting)
        .then(rows=>{

           rows.forEach(row=>{
              if(!byWebhosting[row.db_webhosting]) {
                byWebhosting[row.db_webhosting] = { c: 0, s: 0, db_webhosting: row.db_webhosting, db_user_id: row.db_user_id }
                forWebhosting[row.db_webhosting] = {wh_tally_db_storage:{}}
              }

              byWebhosting[row.db_webhosting].c++
              byWebhosting[row.db_webhosting].s+= row.db_tally_mb
              forWebhosting[row.db_webhosting].wh_tally_db_storage[row.db_database_name] = row.db_tally_mb
           })

           var aggregated = []
           Object.keys(byWebhosting).forEach(wh_id=>{
             aggregated.push(byWebhosting[wh_id])
           })

           return Promise.resolve({forWebhosting:forWebhosting, aggregatedStats: aggregated})
        })
  }


  re.QueryAllowedDbms = function(in_data) {
     var d
     var dbsSofar = 0

     return vali.async(in_data, {webhosting: {isInteger: {lazy:true}}})
       .then(ad=>{
            d = ad

            return getDatabasesBy({db_webhosting: d.webhosting})

       })
       .then(rows=>{
            // console.log("!!! dbs so far", d, rows)
            dbsSofar = rows.length

            return app.MonsterInfoWebhosting.GetInfo(d.webhosting)
       })
       .then(webhosting=>{
            // console.log("!!! wh:",webhosting, dbsSofar)
            if(dbsSofar >= webhosting.template.t_db_max_number_of_databases) return Promise.resolve([])

            var dbms = getDbmsLibInstance()

            return dbms.GetDbms()
             .then(dbmsData=>{
                var re = dbmsData.map(dbmsInstance=>dbmsInstance.dm_dbms_instance_name)

                return Promise.resolve(re)
             })


       })
  }

	re.GetDatabases = function(){
		 return getDatabasesBy({})
	}

  re.GetWebhostingIds = function(){
     return knex.raw("SELECT DISTINCT db_webhosting FROM databases")
  }

  function shouldBeOneRow(prom) {
     return prom.then(rows=>{
           if(rows.length != 1) throw new MError("DATABASE_NOT_FOUND")

           return Promise.resolve(rows[0])
     })
  }

  function GetWebhostingStat(webhosting) {
     const WsLib = require("lib-webhostingstat.js")
     var ws = WsLib(app, knex)
     return ws.GetWebhostingStat(webhosting)
       .catch(ex=>{
          // stats might not be ready at this point, lets be more robust instead
          return Promise.resolve({})
       })
  }

  re.GetDatabaseById = function(id){
    return shouldBeOneRow(getDatabasesBy({db_id: id}))
  }

  re.GetDatabaseByDbmsAndName = function(dbms_id, database_name){

     return shouldBeOneRow(getDatabasesBy({db_dbms: dbms_id, db_database_name: database_name}))
  }

  re.GetDatabaseByWebhostingAndName = function(webhosting, database_name){
     return shouldBeOneRow(getDatabasesBy({db_webhosting: webhosting, db_database_name: database_name}))
  }
  re.GetDatabasesByWebhosting = function(webhosting){
     return getDatabasesBy({db_webhosting: webhosting})
  }
  re.GetAllDatabasesGroupedByWebhosting = function(){
     var xre = {}
     return re.GetDatabases()
       .then(dbs=>{
          dbs.forEach(db=>{
             if(!xre[db.db_webhosting]) xre[db.db_webhosting] = []

             xre[db.db_webhosting].push(db)
          })

          Object.keys(xre).forEach(key=>{
             var rows = xre[key]
             Common.AddCleanupSupportForArray(rows)
          })

          return Promise.resolve(xre)
       })
  }

	re.GetDatabasesByDbms = function(dbms_id){
     return getDatabasesBy({db_dbms: dbms_id})
	}

	re.Insert = function(in_data, req){

      var id
      var d
      var dbmsData
      // console.log("inserting stuff", in_data)
      return vali.async(in_data, {
        "db_database_name": app.getDbnameConstraints(),
        "db_webhosting": {presence: true, isInteger: {lazy:true}},
        "db_user_id": {presence: true, isValidUserId: { info: app.MonsterInfoAccount }},
        "db_dbms": {presence:true, isString: true, lookupDbms: {getDbmsLibInstance:getDbmsLibInstance}},
        "db_tally_mb": {numericality:true},
        "db_locked": {isBooleanLazy:true},
      })
    	.then(ad=>{

          d = ad

          dbmsData = d.db_dbms
          d.db_dbms = dbmsData.dm_id

          return GetWebhostingStat(d.db_webhosting)
      })
      .then(stat=>{

          if(stat.ws_locked) throw new MError("WEBHOSTING_IS_LOCKED")

          return re.QueryAllowedDbms({webhosting:d.db_webhosting})
      })
      .then(allowedDbmsInstances=>{


          // with force flag we can bypass these limitations:
          if(in_data.force) return Promise.resolve()

          if(allowedDbmsInstances.indexOf(in_data.db_dbms) < 0)
              throw new MError("SPECIFIED_DBMS_NOT_ALLOWED_OR_QUOTA_EXHAUSTED")

          return re.GetDatabaseByDbmsAndName(d.db_dbms, d.db_database_name)
            .then(db=>{
               throw new MError("ALREADY_EXISTS", null, {conflictingDatabase: db})
            })
            .catch(ex=>{
                // this means it does not exist, so we are good
                if(ex.message == "DATABASE_NOT_FOUND")
                   return Promise.resolve() // good!

                throw ex
            })
       })
      .then(()=>{
          return re.GetDatabaseByWebhostingAndName(d.db_webhosting, d.db_database_name)
            .then(db=>{
               throw new MError("ALREADY_EXISTS")
            })
            .catch(ex=>{
                // this means it does not exist, so we are good
                if(ex.message == "DATABASE_NOT_FOUND")
                   return Promise.resolve() // good!

                throw ex
            })

      })
       .then(()=>{


           return knex.transaction(trx=>{
               return trx
                 .insert(d)
                 .into("databases")
                 .then(ids=>{
                    return Promise.resolve(ids[0])
                 })
                 .catch(ex=>{
                    if(ex.code == 'SQLITE_CONSTRAINT')
                       throw new MError("ALREADY_EXISTS")
                    throw ex
                 })
                  .then((aid)=>{
                     id = aid
                     app.InsertEvent(req, {
                        e_event_type: "database-create",
                        e_user_id: d.db_user_id,
                        e_other: {db_id:id, db_database_name: in_data.db_database_name}
                     });

                     if(in_data.skip_backend) return Promise.resolve()

                     return dbmsData.GetBackend()
                       .then(backend=>{
                          return backend.CreateDatabase(d.db_database_name)
                       })
                       .catch(ex=>{
                           console.error("error creating database:", ex)
                           if(in_data.force) return Promise.resolve()

                           throw ex
                       })

                  })
           })


        })
        .then(()=>{

           return Promise.resolve(id)
        })

	}

	return re


    function dbRowToObject(database){

      Array("Repair","Export").forEach(c=>{
          database[c] = function(params, emitter, req){
              return database.GetBackend()
                .then(backend=>{
                    return backend[c](database, params, emitter)
                })
                .then(h=>{
                   app.InsertEvent(req, {e_event_type:"database-"+c, e_other:true});
                   return h;
                })
          }
      })

      database.Import = function(in_data, emitter, req){
          var wasAdded = false
          var tmpUser = {
            "force": true,
          }


          var params
          const TokenLib = require("Token")
          var p

          var aconstraints = {
            filename:{presence:true, isFilename: true},
            filemanTask: {format:{pattern:/^[a-f0-9]+$/}},
            dbmsTask: {format:{pattern:/^[a-f0-9]+$/}},
            source_server: {isString:{strictName: true}},
          }


          return vali.async(in_data, aconstraints)
          .then(ain_data=>{
             params = ain_data
             return TokenLib.GetTokenAsync(4, "hex")
          })
          .then(user=>{
             tmpUser.u_username = user
             return TokenLib.GetTokenAsync(8, "base64")
          })
          .then(pass=>{
             tmpUser.u_password = pass


             return database.AddUser(tmpUser)
          })
          .then(()=>{
              wasAdded = true

              return database.GetBackend()
          })
          .then(backend=>{

              return backend.Import(database, tmpUser, params)
          })
          .then((importTask)=>{

              app.InsertEvent(req, {e_event_type:"database-Import", e_other:true});

              if((!params.filemanTask)&&(!params.dbmsTask))
                 return Promise.resolve(importTask)


               var emitter = app.commander.EventEmitter()
               return emitter.spawn()
                 .then(emitterTask=>{

                     const errStr = "error during pipeing the fileman download task to the importTask"

                     // we have to fetch the file first
                     var source;
                     if(params.filemanTask)
                       source = app.pipeFilemanRequest({id: params.filemanTask}, null, "source");
                     else
                     if(params.dbmsTask)
                       source = app.pipeDbmsRequest({id: params.dbmsTask}, null, "source", params.source_server);

                     // then upload it internally
                     var dest = app.pipeLocalRequest(importTask, "post", "dest")


                     source
                       .pipe(dest)

                     emitter.relayRequest(app.pipeLocalRequest(importTask, "output", "output"))

                     importTask.executionPromise
                       .then(()=>{
                          console.log("importtask has finished successfully")
                          emitter.close()
                       })
                       .catch(ex=>{
                          console.error("Something failed", ex);
                          emitter.close(1)
                       })


                     return Promise.resolve(emitterTask)
                 })

          })
          .catch(ex=>{
              console.error("error during database import preparation", ex)

              deleteTempUser()

              throw ex
          })
          .then(h=>{
              h.executionPromise
               .catch(ex=>{ console.error("Something failed", ex); })
               .then(()=>{
                  deleteTempUser()
               })

              // console.log("crapppy", h)

              return Promise.resolve(h)
          })

          function deleteTempUser() {
              if(!wasAdded)  return

               // removing the tmp user
               return database.GetUser(tmpUser.u_username)
                .then(dbuser=>{
                    return dbuser.Delete()
                })

          }
      }


    	database.Cleanup = function(){
    		delete database.db_id
        delete database.db_dbms

        Object.keys(database).forEach(k=>{
           if((!k.match(/^dm_/))||(k.match(/^(dm_dbms_instance_name|dm_dbms_type)$/))) return
           delete database[k]
        })
    		return database
    	}

      database.GetBackend = function(){
                  var dbms = getDbmsLibInstance()
                  return dbms.GetDbmsById(database.db_dbms)
                    .then(dbmsData=>{
                       return dbmsData.GetBackend()
                    })
      }

      database.DeleteAll = function(in_data, emitter, req){
         return database.GetUsers()
           .then(users=>{
              var ps = []
              users.forEach(user=>{
                 ps.push(user.Delete(in_data, emitter, req))
              })
              return Promise.all(ps)
           })
           .then(()=>{
              return database.Delete(in_data, emitter, req)
           })
      }

    	database.Delete = function(in_data, emitter, req){

        		return knex("dbusers").count('u_id AS c')
        		  .whereRaw("u_database=?", [database.db_id])
        		  .then(rows=>{
        		  	  if(rows[0].c) throw new MError("NOT_EMPTY")

        	          return knex("databases")
        	           .whereRaw("db_id=?", [database.db_id])
        	           .del()

        		  })
              .then(()=>{
                 app.InsertEvent(req, {
				   e_event_type: "database-remove",
				   e_user_id: database.db_user_id,
				   e_other: {db_database_name:database.db_database_name}}
				 )
              })
               .then(()=>{

                  if(in_data.skip_backend) return Promise.resolve()

                  return database.GetBackend()
                    .then(backend=>{
                       return backend.DeleteDatabase(database.db_database_name)
                    })
                    .catch(ex=>{
                        console.error("error deleting database", ex)
                        if(in_data.force) return Promise.resolve()
                        throw ex
                    })

               })
    	}

      database.GrantRevokeAllUsers = function(exceeded) {

          return database.GetUsers()
            .then(users=>{
                var ps = []
                users.forEach(user=>{
                   var fn = exceeded ? user.ToReadOnly : user.ToReadWrite
                   ps.push(
                     fn()
                       .catch(ex=>{
                          console.error("could not change readonly of user", user.u_username, ex)
                       })
                   )
                })
                return Promise.all(ps)
            })
            .then(()=>{
                return database.Change({db_locked:exceeded})
            })

      }

      database.Change = function(in_data){
           return vali.async(in_data, {db_locked:{isBooleanLazy: true}})
             .then(ad=>{
                  d = ad

                  if(Object.keys(d).length <= 0) throw new MError("NOTHING_TO_CHANGE")

                  return knex('databases')
                    .whereRaw('db_id=?', [database.db_id])
                    .update(d)
                    .then(()=>{
                       return Promise.resolve() // good!
                    })
             })
      }

      database.Move = function(in_data){
           return vali.async(in_data, {
              dest_u_id: {presence: true, isValidUserId: { info: app.MonsterInfoAccount }},
              dest_wh_id: {presence: true, isInteger: {lazy:true}},
           })
           .then(d=>{

                return knex('databases')
                  .whereRaw('db_id=?', [database.db_id])
                  .update({
                    "db_webhosting": d.dest_wh_id,
                    "db_user_id": d.dest_u_id,
                  })
                  .then(()=>{

                      return knex('dbusers')
                        .whereRaw('u_database=?', [database.db_id])
                        .update({
                          "u_webhosting": d.dest_wh_id,
                          "u_user_id": d.dest_u_id,
                        })
                  })
                  .return()
           })
      }

      database.GetUsers = function(){
         var dbusers = dbUsersLibInstance()
         return dbusers.GetUsers()
      }
      database.AddUser = function(in_data, req){
         var dbusers = dbUsersLibInstance()
         return dbusers.Insert(in_data, req)
      }
      database.GetUser = function(name){
         var dbusers = dbUsersLibInstance()
         return dbusers.GetUserByName(name)
      }

    	return database

      function dbUsersLibInstance(){
        return require("lib-dbusers.js")(app, knex, database)
      }

    }

    function getDbmsLibInstance(){
       const DbmsLib = require("lib-dbms.js");
       return DbmsLib(app, knex);
    }

}
