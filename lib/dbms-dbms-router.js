module.exports = function(app, knex) {

    var router = app.ExpressPromiseRouter()

    const DbmsLib = require("lib-dbms.js")
    const DatabasesLib = require("lib-databases.js")

    var dbms = DbmsLib(app, knex)

  router.post("/:dbmsname/test", getDbmsAndBackendByName, function(req,res,next){
          return req.sendPromResultAsIs(
              req.backend.Test()
          )

     })

  router.get("/:dbmsname/scoreboard", getDbmsAndBackendByName, function(req,res,next){
          return req.sendPromResultAsIs(
             req.backend.GetScoreboard()
          )
     })

  router.post("/:dbmsname/repair", getDbmsAndBackendByName, function(req,res,next){
          return req.sendTask(
             req.backend.RepairAllDatabases()
          )

     })

    function getDbmsByName(req,res,next){
       return dbms.GetDbmsByName(req.params.dbmsname)
         .then(dbms=>{
            req.dbms = dbms
            next()
         })
    }

    function getDbmsAndBackendByName(req,res,next) {
       return getDbmsByName(req,res, function(err){
          if(err) return next(err)
          var backendProm= req.dbms.GetBackend()
          return backendProm.then(backend=>{
                req.backend = backend
                return next()
            })
       })
    }


  router.get("/:dbmsname/database/:database/users", getDbmsByName, function(req,res,next){
          var databases = DatabasesLib(app, knex)
          return req.sendPromResultAsIs(
             databases.GetDatabaseByDbmsAndName(req.dbms.dm_id, req.params.database)
               .then(databaseData=>{
                 return databaseData.GetUsers()
               })
               .then(users=>{
                 return Promise.resolve(users.Cleanup())
               })
          )

  })

  router.route("/:dbmsname/database/:database")
     .get(getDbmsByName, function(req,res,next){
          var databases = DatabasesLib(app, knex)
          return req.sendPromResultAsIs(
             databases.GetDatabaseByDbmsAndName(req.dbms.dm_id, req.params.database)
               .then(databaseData=>{
                  return Promise.resolve(databaseData.Cleanup())
               })
          )
     })
     .delete(getDbmsByName, function(req,res,next){

         return knex.transaction(function(trx){
             var databases = DatabasesLib(app, trx)

             return req.sendOk(
                 databases.GetDatabaseByDbmsAndName(req.dbms.dm_id, req.params.database)
                   .then(databaseData=>{
                      return databaseData.DeleteAll(req.body.json)
                   })
              )
         })

     })


  router.get("/:dbms/databases", function(req,res,next){

          return req.sendPromResultAsIs(
             dbms.GetDbmsByName(req.params.dbms).then(dbmsData=>{
               return dbmsData.GetDatabases()
             })
             .then(dbs=>{
               return Promise.resolve(dbs.Cleanup())
             })
          )

  })

  router.route("/:dbms")
     .get(function(req,res,next){
          return req.sendPromResultAsIs(
             dbms.GetDbmsByName(req.params.dbms).then(dbmsData=>{
               return Promise.resolve(dbmsData.Cleanup())
             })
          )
     })
     .post(function(req,res,next){
          return req.sendOk(
             dbms.GetDbmsByName(req.params.dbms)
               .then(dbmsData=>{
                   return dbmsData.Change(req.body.json)
               })
          )
     })
     .delete(function(req,res,next){

         return knex.transaction(function(trx){
             var dbms = DbmsLib(app, trx)

             return req.sendOk(
                 dbms.GetDbmsByName(req.params.dbms)
                   .then(dbmsData=>{
                      return dbmsData.Delete(req)
                   })
              )
         })

     })

  router.route("/")
     .put(function(req,res,next){

         return req.sendOk(
            knex.transaction(function(trx){
             var dbms = DbmsLib(app, trx)
             return dbms.Insert(req.body.json, req)
           })
         );

     })
     .get(function(req,res,next){
          return req.sendPromResultAsIs(
             dbms.GetDbms()
               .then(dbmsData=>{
                  return Promise.resolve(dbmsData.Cleanup())
               })
           )

      })



  return router



}
