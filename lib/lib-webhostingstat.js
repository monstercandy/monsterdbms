var lib = module.exports = function(app, knex){

    const KnexLib = require("MonsterKnex")

    const MError = require("MonsterError")

    var ValidatorsLib = require("MonsterValidators")

  	var vali = ValidatorsLib.ValidateJs()
    var Common = ValidatorsLib.array()

	var re = {}

  function getWebhostingStatsBy(filterHash) {

    var f = KnexLib.filterHash(filterHash)

    return knex("webhostingstats").select()
      .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
      .then(rows=>{
              rows.map(row => dbRowToObject(row))
              Common.AddCleanupSupportForArray(rows)

              return Promise.resolve(rows)
      })

  }

  function GetDbmsLibInstance(){
      const DbmsLib = require("lib-dbms.js")
      var dbms = DbmsLib(app, knex)
      return dbms;
  }

  function GetDatabasesLibInstance(){
      const DatabasesLib = require("lib-databases.js")
      var databases = DatabasesLib(app, knex)
      return databases
  }

  re.GetScoreboards = function(){
         var dbmsLib = GetDbmsLibInstance()
         var re = []
         return dbmsLib.GetDbms()
          .then(dbmses=>{
              var ps = []
              dbmses.forEach(dbms=>{
                 ps.push(
                   dbms.GetBackend()
                     .then(backend=>{
                        return backend.GetScoreboard()
                     })
                     .then(data=>{
                        data.forEach(l=>{
                            l.dm_dbms_instance_name = dbms.dm_dbms_instance_name;
                            re.push(l)
                        })
                        // console.log("scoreboard call just returned!", data, dbms.dm_dbms_instance_name)
                     })
                 )
              })
              return Promise.all(ps)
          })
          .then(()=>{
             return Promise.resolve(re)
          })

  }

  re.QueryLiveDbmsStatistics = function(){
      var dbms = GetDbmsLibInstance()
      var re = {}
      return dbms.GetDbms()
        .then(dbmsData=>{
            var ps = []
            dbmsData.forEach(dbms=>{
               ps.push(
                dbms.QueryLiveStatistics()
                .then(stats=>{
                    re[dbms.dm_dbms_instance_name] = stats
                })
                .catch(ex=>{
                   console.error("error while querying dbms stats", dbms.dm_dbms_instance_name, ex)
                })
               )
            })

            return Promise.all(ps)
        })
        .then(()=>{
            return Promise.resolve(re)
        })
  }

  re.StoreDbmsStatistics = function(stats, onlyWebhosting, req){
    /*
    stats looks something like this:
    {
        "dbms_instance_name": { "database_name1": 123456, "database_name1": 234567 }
    }
    */

      var lockedOnesBefore = {}
      var dbmsByName = {}
      var dbms = GetDbmsLibInstance();

      var previousStats = {};

      return vali.async({onlyWebhosting: onlyWebhosting}, {
        "onlyWebhosting": {isInteger: true},
      })
      .then(d=>{
         onlyWebhosting = d.onlyWebhosting

         return dbms.GetDbms()
      })
        .then(dbmsData=>{

            var ps = []

            dbmsData.forEach(dbms=>{
               dbmsByName[dbms.dm_dbms_instance_name] = dbms

               var dbmsStats = stats[dbms.dm_dbms_instance_name]
               if(!dbmsStats) {
                  console.error("We dit not receive stats for ", dbms.dm_dbms_instance_name)
                  return
               }

               ps.push(dbms.StoreStatistics(dbmsStats))
            })

            // this is just for sanity checking:
            Object.keys(stats).forEach(name=>{
                if(!dbmsByName[name]) {
                   console.error("We received stats for a dbms we do not have", name)
                   return
                }
            })

            return Promise.all(ps)
        })
        .then(()=>{

            return knex("webhostingstats").select().whereRaw("ws_locked=?",[true])
        })
        .then((rows)=>{
            rows.forEach(row=>{
               lockedOnesBefore[row.ws_webhosting] = true
            })

            var whereStr = "1"
            var filter = []

            if(onlyWebhosting) {
               whereStr = "ws_webhosting=?"
               filter = [onlyWebhosting]
            }

            return knex("webhostingstats").select().whereRaw(whereStr, filter)
              .then(rows=>{

                 rows.forEach(row=>{
                    previousStats[row.ws_webhosting] = row.ws_tally_mb;
                 });

                 return knex("webhostingstats").whereRaw(whereStr, filter).del();
              })

        })
        .then(()=>{
           // query for aggregated results

           var databases = GetDatabasesLibInstance()


           return databases.QueryAggregatedStatsWithDetails(onlyWebhosting)
        })
        .then(stats =>{

           // this is started in the background, we are not particularly intersted in the result
           var c = stats.forWebhosting
           c.wh_tally_db_storage_flush= true
           app.GetWebhostingMapiPool().postAsync("/s/[this]/webhosting/tallydetails",c)


          /*
          [ {
    db_webhosting: 12345,
    c: 2,
    s: 0 } ]
    */

            var ps = []
            stats.aggregatedStats.forEach(statRow=>{

                console.log("aggregatedStats processing", statRow)

                var maxSizeMb, actSizeMb, exceeded, wasExceededAlready, consumptionIsClose, ws_locked, whInfo

                ps.push(
                   app.MonsterInfoWebhosting.GetInfo(statRow.db_webhosting)
                     .then(info=>{
                         whInfo = info
                         maxSizeMb = info.template.t_db_max_databases_size_mb
                         actSizeMb = app.roundToTwo(statRow.s)

                         exceeded = (actSizeMb >= maxSizeMb)
                         wasExceededAlready = lockedOnesBefore[statRow.db_webhosting] ? true : false

                         consumptionIsClose = actSizeMb > (maxSizeMb * app.config.get("comsumption_is_close_multiplier"))

                         console.log("dbms webhosting stat", statRow.db_webhosting, actSizeMb, maxSizeMb, exceeded)

                         ws_locked = exceeded

                         return rawInsert({
                            ws_user_id: statRow.db_user_id,
                            ws_webhosting: statRow.db_webhosting,
                            ws_locked: ws_locked,
                            ws_tally_mb: actSizeMb,
                            ws_database_count: statRow.c,
                         })

                     })
                     .then(()=>{
                         if(
                              (exceeded != wasExceededAlready)
                              &&
                              (app.config.get("grant_revoke_permissions_on_violation"))
                           )
                         {
                            var databases = GetDatabasesLibInstance()
                            return databases.LockUnlockDatabasesBasedOnExceeding(statRow.db_webhosting, exceeded)
                         }

                         return Promise.resolve()
                     })
                     .then(()=>{
                         // sending email notification if the storage consumption is higher than the allowance or close to the allowance
                         if(
                             (consumptionIsClose) && 
                             (app.config.get("send_consumption_warning_email")) && 
                             (previousStats[statRow.db_webhosting] != actSizeMb)
                           )
                         {
                               var mailer = app.GetMailer()

                               var x = {}
                               x.toAccount = {user_id: whInfo.wh_user_id, emailCategory: "TECH"}
                               x.template = "dbms/consumption-warning"
                               var dueDate = app.config.get("due_date_before_write_revocation")
                               x.context = {wh: {wh_name:whInfo.wh_name}, stats: {maxSizeMb: maxSizeMb, actSizeMb: actSizeMb}, "due_date_before_write_revocation": dueDate}

                               return mailer.SendMailAsync(x)
                         }

                        return Promise.resolve()

                     })
                     .catch(ex=>{
                        console.error("Unable to complete statistics processing for", statRow, ex)
                     })
                )
            })

            app.InsertEvent(req, {e_event_type: "dbms-store-stats", e_other: true});
            return Promise.all(ps)

        })


  }


  function rawInsert(d) {
           return knex
             .insert(d)
             .into("webhostingstats")
             .then(ids=>{
                return Promise.resolve(ids[0])
             })

  }

  re.Insert = function(webhosting){

     return vali.async(in_data, {
        "ws_webhosting": {presence: true, isString: {strictName: true}},
        "ws_locked": {presence: true, isBooleanLazy: true},
        "ws_tally_mb": {presence: true, isDecimal: true},
      })

     .then(d=>{

        return rawInsert(d)

     })

  }

  re.GetWebhostingStats = function(){
     return getWebhostingStatsBy({})
  }


	re.GetWebhostingStat = function(webhosting){
    return getWebhostingStatsBy({ws_webhosting:webhosting})
		  .then(rows=>{
          if(rows.length != 1) throw new MError("STATS_NOT_FOUND")

          return Promise.resolve(rows[0])
		  })
	}

	return re


    function dbRowToObject(stat){

    	stat.Cleanup = function(){
        delete stat.ws_id
    		return stat
    	}

      stat.Delete = function(){
            return knex("webhostingstats")
               .whereRaw("ws_id=?", [stat.ws_id])
               .del()
      }

    	stat.Change = function(){
           return vali.async(in_data, {ws_locked:{isBooleanLazy: true}})
             .then(ad=>{
                  d = ad

                  if(Object.keys(d).length <= 0) throw new MError("NOTHING_TO_CHANGE")

                  return knex('webhostingstats')
                    .whereRaw('ws_id=?', [stat.ws_id])
                    .update(d)
                    .then(()=>{
                       return Promise.resolve() // good!
                    })
             })

    	}

    	return stat

    }

}
