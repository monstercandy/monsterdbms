module.exports = function(app, knex) {

    var router = app.ExpressPromiseRouter()


    router.get("/supported/dbmstypes", function(req,res,next){
       req.sendResponse(require("lib-dbms.js").SupportedDbmsTypes())
    })

    router.post("/supported/databases", function(req,res,next){
       const DatabasesLib = require("lib-databases.js")
       return req.sendPromResultAsIs(
            DatabasesLib(app, app.knex).QueryAllowedDbms(req.body.json)
         )
    })


  return router



}
