module.exports = function(app, knex) {

    var router = app.ExpressPromiseRouter()

    const WebhostingLib = require("lib-webhostingstat.js")
    var ws = WebhostingLib(app, app.knex)

    router.get("/", function(req,res,next){
        return req.sendPromResultAsIs(
             ws.GetWebhostingStats()
          )
    })

    router.get("/scoreboard", function(req,res,next){

         return req.sendPromResultAsIs(
            ws.GetScoreboards()
         )

     })


    router.post("/get-and-process", function(req,res,next){

        return req.sendPromResultAsIs(
            getAndProcess(req.body.json.onlyWebhosting, req)
        )

    })

    router.get("/live", function(req,res,next){
        return req.sendPromResultAsIs(
            ws.QueryLiveDbmsStatistics()
        )
    })



    router.post("/:webhosting", function(req,res,next){
       return req.sendOk(
            ws.GetWebhostingStat(req.params.webhosting)
              .then(stat=>{
                  return stat.Change(req.body.json)
              })
         )
    })

    const cron = require('Croner');
    var statProcessingInProgress = 0

    var csp = app.config.get("cron_stat_processing")
    if(csp) {
        cron.schedule(csp, function(){
            return router.cronWorks()
        });
    }


    router.cronWorks = function(){

            return getAndProcess()
    }


  return router




  function getAndProcess(onlyWebhosting, req){

      return knex.transaction(function(trx){ // transaction is needed here to speed up things
          var ws = WebhostingLib(app, trx)

          statProcessingInProgress++
          return ws.QueryLiveDbmsStatistics()
            .then(stats=>{
                return ws.StoreDbmsStatistics(stats, onlyWebhosting, req)
            })
            .then(x=>{
                statProcessingInProgress--
                return Promise.resolve(x)
            })

      })


  }

}
