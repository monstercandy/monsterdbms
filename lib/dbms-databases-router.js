module.exports = function(app, knex) {

    var router = app.ExpressPromiseRouter()

    const DatabasesLib = require("lib-databases.js")
    var databases = DatabasesLib(app, knex)
    const dotq = require("MonsterDotq");

  function getDatabaseBy(req, databasesi) {
     if(!databasesi) databasesi = databases
     return databasesi.GetDatabaseByWebhostingAndName(req.params.webhosting, req.params.database)
       .then(db=>{
          req.db = db
          return Promise.resolve(db)
       })
  }

  function getDatabaseExpress(req,res,next) {
     return getDatabaseBy(req)
       .then(()=>{
          return next()
       })
  }

  function getDatabaseUserBy(req) {
     return req.db.GetUser(req.params.dbuser)
       .then(dbuser=>{
          req.dbuser = dbuser
          return Promise.resolve(dbuser)
       })
  }
  function getDatabaseUserExpress(req,res,next) {
     return getDatabaseUserBy(req)
       .then(()=>{
          return next()
       })
  }
  function getDatabaseTrx(cb) {
     return function(req,res,next){
        return req.sendOk(
           knex.transaction(function(trx){
              var databases = DatabasesLib(app, trx)
             return getDatabaseBy(req, databases)
               .then(()=>{
                  return cb(req,res,next)
               })
           })
        );
     }
  }
  function getDatabaseUserTrx(cb) {
     return getDatabaseTrx(function(req,res,next){
        return getDatabaseUserBy(req)
          .then(()=>{
             return cb(req,res,next)
          })

     })
  }

    router.get("/backupdirs", function(req,res,next){
      return req.sendPromResultAsIs(
          databases.QueryBackupDirs()
      )
    })

    router.get("/stats", function(req,res,next){
      return req.sendPromResultAsIs(
          databases.QueryAggregatedStats()
      )
    })

    router.get("/stats-detailed", function(req,res,next){
      return req.sendPromResultAsIs(
          databases.QueryAggregatedStatsWithDetails()
      )
    })


  function dbbackupWork(prom, gemitter){
     const fs = require("fs")
     const MonsterMoment = require("MonsterMoment")

     var ts_begin = new MonsterMoment();
     var stats = {
        success: 0,
        fail: 0,
        databases: 0,
        storages: 0,
        users: 0,    
        begin: ts_begin.toISOStringWithTimeZone(),
     };

     var users = {};

     return prom.then(fullStuffToProcess=>{
        const path = require("path")

        var webhostings = Object.keys(fullStuffToProcess)

        var relayer = app.GetRelayerMapiPool()

        return dotq.linearMap({array: webhostings, action: webhostingId=>{
           if(gemitter) gemitter.send_stdout_ln("Starting to work on "+webhostingId)

           stats.storages++;

           var dbs = fullStuffToProcess[webhostingId];

           return app.MonsterInfoWebhosting.GetInfo(webhostingId)
            .then(wh=>{

                 if(!users[wh.wh_user_id]) {
                    users[wh.wh_user_id];
                    stats.users++;
                 }

                 var keepDays = wh.template.t_db_keep_backup_for_days
                 if(typeof keepDays != "string") keepDays = parseInt(keepDays,10)
                 if(typeof keepDays != "number") keepDays = 3

                 if(keepDays <= 0) {
                    if(gemitter) gemitter.send_stdout_ln("Dbbackup is disabled for this webhosting")

                    throw new Error("Dbbackup keep days is disabled")
                 }

                 return backupDatabases(wh, dbs)
                   .then(()=>{
                      return deleteOldDumps(wh, keepDays);
                   })

            })
            .catch(ex=>{
               if(gemitter) gemitter.send_stdout_ln("Error while querying for this webhosting")
               console.error("Error while querying for webhosting", ex)
            })
        }})
        .then(()=>{
            var ts_end = new MonsterMoment();
            stats.end = ts_end.toISOStringWithTimeZone();
            stats.duration_sec = MonsterMoment.base.duration(ts_end.diff(ts_begin)).asSeconds();
            var mailer = app.GetMailer()

            var x = {}
            x.toAdmin = true;
            x.template = "dbms/backup-stats"
            x.context = {stats: stats};
            mailer.SendMailAsync(x).catch(console.error);

            if(gemitter) {
              gemitter.send_stdout_ln("Ready.")
              gemitter.close()
            }          
        })


        function deleteOldDumps(wh, keepDays){

           if(gemitter) gemitter.send_stdout_ln("Deleting database dumps older than "+keepDays+" days")

           relayer.postAsync("/s/[this]/fileman/"+wh.wh_id+"/find", {"path":"/dbbackup/","mtime":"+"+keepDays,"delete":true})

        }

        function backupDatabases(wh, dbs){
           return dotq.linearMap({array: dbs, action: db=>{
               stats.databases++;
               console.log("Database export begin:",db.db_database_name);
               if(gemitter) gemitter.send_stdout_ln("Database: "+db.db_database_name)

               var dbmsExport;
               return db.Export()
                 .then(adbmsExport=>{
                    dbmsExport = adbmsExport;

                    var now = MonsterMoment.nowRaw()

                    var filename = db.db_database_name+"-"+now+".sql.gz"

                    if(gemitter) gemitter.send_stdout_ln("Saving as: "+filename)

                    var destfile = path.join("/dbbackup", filename)

                    console.log("saving database as", destfile)

                    return new Promise(function(resolve,reject){

                        relayer.postAsync("/s/[this]/fileman/"+wh.wh_id+"/upload", {"dst_full_path":destfile})
                          .then(u=>{
                              console.log("fileman just responded a stream handle to the upload request", db.db_database_name);

                              var uploadToFileman = app.pipeFilemanRequest(u.result, "POST", errhandler("uploadToFileman"))

                              var outputReader = app.pipeFilemanRequest(u.result, "output")
                              Array("end").forEach(type=>{ // "finish","close",
                                outputReader.on(type, function(){
                                   // this event is triggered when the upload process finishes really
                                   console.log("output reader of upload of ",db.db_database_name,"triggered an event:", type);
                                   resolve();
                                })
                              })
                              outputReader.on("data", function(msg){
                                 if(gemitter) gemitter.send_stdout_ln(msg)
                              })

                              var uploadStream = app.pipeLocalRequest(dbmsExport, null, errhandler("dbmsExport")).pipe(uploadToFileman);
                              Array("end").forEach(type=>{ // "finish","close",
                                uploadStream.on(type, function(){
                                   console.log("uploading database export stream of",db.db_database_name,"triggered an event:", type);
                                })
                              });
                          })
                          .then((x)=>{
                             stats.success++;
                             return x;
                          })
                          .catch(reject);

                          function errhandler(cat){
                             const errStr = "Error during piping the dbms export task to fileman upload task"
                             return function(err){
                                var fullErr = cat+": "+errStr;
                                if(gemitter) gemitter.send_stdout_ln("ERROR: "+fullErr);
                                console.error(fullErr, err);
                                stats.fail++;
                                reject(err);
                             }
                          }


                    })

                 })
                 .then(()=>{
                    console.log("Database exported successfully", db.db_database_name);
                    if(gemitter) gemitter.send_stdout_ln("Database exported successfully.")
                 })
                 .catch(ex=>{
                    if(dbmsExport) {
                       console.log("Aborting database export task", dbmsExport.id);
                       app.commander.abortTask(dbmsExport.id);
                    }
                    if(gemitter) gemitter.send_stdout_ln("Error during database export")
                    console.error("Error while exporting database", db, ex)
                 })

           }})

        }

     })
  }

  function dbbackupWorkExpress(req,res,next){

     return getEmitter()
       .then(h=>{
           req.sendTask(Promise.resolve(h))
           dbbackupWork(req.dbbackupProm, h.emitter)
       })
  }

  router.post("/dbbackup", function(req,res,next){
     req.dbbackupProm = databases.GetAllDatabasesGroupedByWebhosting()
     next()
  }, dbbackupWorkExpress)

  router.post("/:webhosting/dbbackup", function(req,res,next){
     req.dbbackupProm = new Promise(function(resolve,reject){
        return databases.GetDatabasesByWebhosting(req.params.webhosting)
          .then(dbs=>{
             var re = {}
             re[req.params.webhosting] = dbs
             resolve(re)
          })
     })
     next()
  }, dbbackupWorkExpress)



    router.use("/:webhosting/slowlog", require("dbms-slowlog-router.js")(app, app.knex));


    router.post("/:webhosting/supported", function(req,res,next){
       return req.sendPromResultAsIs(
            databases.QueryAllowedDbms({webhosting: req.params.webhosting})
         )
    })

  router.post("/:webhosting/:database/user/:dbuser/lock", getDatabaseExpress,getDatabaseUserExpress, function(req,res,next){
      return req.sendOk(req.dbuser.ToReadOnly(req))
  })
  router.post("/:webhosting/:database/user/:dbuser/unlock", getDatabaseExpress,getDatabaseUserExpress, function(req,res,next){
      return req.sendOk(req.dbuser.ToReadWrite(req))
  })

  router.route("/:webhosting/:database/user/:dbuser")
     .get(getDatabaseExpress,getDatabaseUserExpress, function(req,res,next){

             return req.sendResponse(req.dbuser.Cleanup())
     })
     .delete(getDatabaseUserTrx(function(req,res,next){
             return req.dbuser.Delete(req.body.json, null, req)
     }))

  Array("Import", "Export","Repair").forEach(c=>{
      var r = c.toLowerCase()


      router.post("/:webhosting/:database/"+r, getDatabaseExpress, function(req,res,next){
             return req.sendTask(req.db[c](req.body.json, null, req))
      })

  })

  router.route("/:webhosting/:database/users")
     .get(getDatabaseExpress, function(req,res,next){

         return req.sendPromResultAsIs(
           req.db.GetUsers()
           .then(dbusers=>{
              return Promise.resolve(dbusers.Cleanup())
           })
         )
     })
     .put(getDatabaseTrx(function(req,res,next){
          return req.db.AddUser(req.body.json, req);
     }))

  router.route("/:webhosting/:database/move")
     .post(getDatabaseExpress, function(req,res,next){

             return req.sendOk(
                req.db.Move(req.body.json)
             )
     })

  router.route("/:webhosting/:database")
     .get(getDatabaseExpress, function(req,res,next){

             return req.sendResponse(req.db.Cleanup())
     })
     .delete(getDatabaseExpress, function(req,res,next){

        deleteDatabasesViaEmitter([req.db], req.body.json, req)

     })
     .post(getDatabaseExpress, function(req,res,next){

             return req.sendOk(
                req.db.Change(req.body.json)
             )
     })

  function deleteDatabasesViaEmitter(databases, params, req){
      return getEmitter()
        .then(h=>{
          var emitter = h.emitter
          if(req)
             req.sendTask(Promise.resolve(h));

          return dotq.linearMap({array: databases, action: db=>{

             var dbname = db.db_database_name
             emitter.send_stdout(`Deleting database: ${dbname}\n`)
             return db.DeleteAll(params, emitter, req)
                .then(()=>{
                    emitter.send_stdout(`Deleting database finished successfully: ${dbname}\n`)
                })

          }})
          .then(()=>{
              emitter.send_stdout("Finished deleting the selected databases.\n")
              emitter.close()
          })
          .catch(ex=>{
              console.error("error during delete database", ex)
              emitter.send_stderr(`Error while deleting database\n`)
              emitter.close(1)
          })

        })
  }

  function getEmitter() {
         var emitter = app.commander.EventEmitter()
         return emitter.spawn()
           .then(h=>{
              h.emitter = emitter
              return Promise.resolve(h)
           })

  }



  router.route("/:webhosting")
     .get(function(req,res,next){

             return req.sendPromResultAsIs(
                databases.GetDatabasesByWebhosting(req.params.webhosting)
                  .then(dbs=>{
                     return Promise.resolve(dbs.Cleanup())
                  })
             )
     })
     .put(function(req,res,next){
         req.body.json.db_webhosting = req.params.webhosting

         return req.sendOk(
            databases.Insert(req.body.json, req)
         )
     })
     .post(function(req,res,next){

         return req.sendOk(
                databases.GetDatabasesByWebhosting(req.params.webhosting)
                  .then(dbs=>{
                      var ps = []
                      dbs.forEach(db=>{
                          ps.push(db.Change(req.body.json))
                      })
                      return Promise.all(ps)
                  })
         )
     })
     .delete(function(req,res,next){

         return databases.GetDatabasesByWebhosting(req.params.webhosting)
            .then(dbs=>{
              return deleteDatabasesViaEmitter(dbs, req.body.json, req)
            })

     })



  router.route("/")
     .put(function(req,res,next){

         return req.sendOk(
            databases.Insert(req.body.json, req)
         )


     })
     .search(function(req,res,next){
          return req.sendPromResultAsIs(
             databases.GetAllDatabasesGroupedByWebhosting()
               .then(databasesData=>{
                  Object.keys(databasesData).forEach(key=>{
                      databasesData[key].Cleanup()
                  })

                  return Promise.resolve(databasesData)
               })
           )

     })
     .get(function(req,res,next){
          return req.sendPromResultAsIs(
             databases.GetDatabases()
               .then(databasesData=>{
                  return Promise.resolve(databasesData.Cleanup())
               })
           )

      })
     .search(function(req,res,next){
          return req.sendPromResultAsIs(
             databases.GetDatabasesBy(req.body.json)
               .then(databasesData=>{
                  return Promise.resolve(databasesData.Cleanup())
               })
           )

     })




    const cron = require('Croner');

    var csp = app.config.get("cron_dbbackup")
    if(csp) {
        cron.schedule(csp, function(){
            return router.cronWorks()
        });
    }

    router.cronWorks = function(){
            return dbbackupWork(databases.GetAllDatabasesGroupedByWebhosting())
    }

  return router



}
