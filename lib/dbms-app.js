// permissions needed: ["FILEMAN_UPLOAD_DBBACKUP","ADMIN_TALLY","EMIT_LOG","INFO_ACCOUNT","INFO_WEBHOSTING","SEND_EMAIL_TO_USER"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "dbms"

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')
    var fs = require("MonsterDotq").fs();

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');



    config.defaults({
      "cleanup": {
          "45 5 * * *": [
            {table: "dbmsslow", agePrefix: "ds_", ageDays: 90},
          ]
       },

      "rotate_slowlog_after_hours": 24,
      "slowlog_sync_flush_seconds": 10,

      "wie_legacy_user_hosts": [],

      "default_charset": "utf8",
      "comsumption_is_close_multiplier": 0.9,
      "grant_revoke_permissions_on_violation": false,
      "send_consumption_warning_email": false,
      "cmd_mysql_import": {"executable":"/opt/MonsterDbms/lib/cmds/gunzip-wrapper.sh",  "args":["[db_webhosting]", "[gunzip]", "[bin_mysql]","-h", "[host]", "-u", "[u_username]", "--password=[u_password]", "[db_database_name]" ]},
      "cmd_mysql_export": {"executable":"/opt/MonsterDbms/lib/cmds/gzip-wrapper.sh",    "args":["[db_webhosting]", "[bin_mysqldump]","-h", "[host]", "-u", "[user]", "--password=[password]", "[db_database_name]" ]},
      "cmd_mysql_repair_table": {"executable":"[bin_mysqlcheck]",  "args":["-h", "[host]", "-u", "[user]", "--password=[password]", "--repair", "--databases", "[db_database_name]" ]},
      "cmd_mysql_repair_db": {"executable":"[bin_mysqlcheck]",  "args":["-h", "[host]", "-u", "[user]", "--password=[password]", "--repair", "--databases", "[db_database_name]", "--tables", "table_name" ]},
      "cmd_mysql_repair_all": {"executable":"[bin_mysqlcheck]",  "args":["-h", "[host]", "-u", "[user]", "--password=[password]", "--repair", "--all-databases" ]},
    })

   Array("mysql").forEach(k=>{
      if(!config.get(k)) throw new Error("Mandatory configuration option is missing: "+k)
    })

    config.appRestPrefix = "/dbms"

    var app = me.Express(config, moduleOptions);
    app.MError = me.Error

    var dbmsBackendCache = {}
    var dbmsBackendProm = {}
    function getDatabaseBackendLibCacheKey(dbms_type, connectionString) {
      return dbms_type+connectionString
    }
    app.getDatabaseBackendLib = function(dbms_type, connectionString) {
        var k = getDatabaseBackendLibCacheKey(dbms_type,connectionString)
        if(dbmsBackendCache[k]) return Promise.resolve(dbmsBackendCache[k])
        if(!dbmsBackendProm[k])
           dbmsBackendProm[k] = require("lib-"+dbms_type+".js")(connectionString, app)
             .then(re=>{
                delete dbmsBackendProm[k]
                dbmsBackendCache[k] = re

                return Promise.resolve(re)
             })
        return dbmsBackendProm[k]
    }
    app.removeDatabaseBackendLib= function(dbms_type, connectionString) {
        var k = getDatabaseBackendLibCacheKey(dbms_type,connectionString)
        if(dbmsBackendCache[k]) {
           if(dbmsBackendCache[k].Teardown)
              dbmsBackendCache[k].Teardown()
           delete dbmsBackendCache[k]
        }
    }


    app.roundToTwo =function(num) {
        return Math.round(num * 100) / 100
    }


    app.knex = require("MonsterKnex")(config)

    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

    var router = app.PromiseRouter(config.appRestPrefix)

    // external service providers will call this route
    router.RegisterPublicApis(["/tasks/"])
    router.RegisterDontParseRequestBody(["/tasks/"])

    const MonsterInfoLib = require("MonsterInfo")

    app.MonsterInfoWebhosting = MonsterInfoLib.webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer})
    app.MonsterInfoAccount = MonsterInfoLib.account({config: app.config, accountServerRouter: app.ServersRelayer})

    // for fileman we do NOT expect any valid internalapi secrets as we gonna call only the public tasks part of it
    app.ServersFileman = router.ServersMiddleware("fileman", 0)

    // relayer will be used to get a fileman task id for the nightly dbbackup uploads, then we will send the stuff directly to fileman

    var commanderOptions = app.config.get("commander") || {}
    commanderOptions.routerFn = app.ExpressPromiseRouter
    app.commander = require("Commander")(commanderOptions)
    router.use("/tasks", app.commander.router)


    router.use("/dbms", require("dbms-dbms-router.js")(app, app.knex))

    var dbsRouter = require("dbms-databases-router.js")(app, app.knex)
    router.use("/databases", dbsRouter)

    router.use("/config", require("dbms-config-router.js")(app, app.knex))

    var statRouter = require("dbms-webstat-router.js")(app, app.knex)
    router.use("/stats", statRouter)

    router.use("/tm", require("dbms-tm-router.js")(app, app.knex))


    var slowlogRouter = require("dbms-slowlog-router.js")(app, app.knex);
    router.use("/slowlog/all", slowlogRouter);
    router.use("/slowlog/by-webhosting/:webhosting", slowlogRouter);

    require("Wie")(app, router, require("lib-wie.js")(app, app.knex));


	app.GetWebhostingMapiPool = app.GetRelayerMapiPool

    app.Prepare = function() {

       return app.knex.migrate.latest()
         .then(()=>{
            app.getSlowlog() // this gonna initialize the slowlog parser
         })
    }

    app.Cleanup = function() {

       var fn = config.get("db").connection.filename
       if(!fn) return Promise.resolve()

       return fs.unlinkAsync(fn).catch(x=>{}) // the database might not exists, which is not an error in this case
        .then(()=>{
            return fs.unlinkAsync(app.config.get("watch_slowlog")["mysql-slow"]).catch(x=>{})
        })

    }

    app.getDbnameConstraints = function() {
       if (process.env.DBNAMELAZY)
          return {presence: true, isString: true}

       return {presence: true, isString: {strictName: true}, format: /^[a-zA-Z0-9\-_]{2,32}$/}
    }

    function setupStreamErrorHandler(stream, cat, errCallbackOrStr) {
        var errStr = ""
        var errCallback
        if(typeof errCallbackOrStr == "string")
          errStr = errCallbackOrStr
        if(typeof errCallbackOrStr == "function")
          errCallback = errCallbackOrStr

        if(!errCallback)
          errCallback = function(err){
            console.error("error during stream operation", stream.url, err, errStr)
          }
        stream.on("error", errCallback)

    }

    app.pipeLocalRequest = function(h, method, errCallbackOrStr) {
        if(!method) method = "get"

        const request = require("RequestCommon")
        var url = "http://127.0.0.1:"+app.config.get("listen_port")+"/dbms/tasks/"+h.id
        if(method == "post")
          url += "/input"
        if(method == "output") {
          method = "get"
          url += "/output"
        }

        console.log("pipeLocalRequest url:", url)

        var re = request[method](url)
        setupStreamErrorHandler(re, "pipeLocalRequest", errCallbackOrStr)
        return re
    }


    app.pipeDbmsRequest = function(h, method, errCallbackOrStr, source_server) {
        var pipeConfig = app.Mapi.cloneServersAsPipe(app.ServersRelayer.FirstServer())
        var mc = app.Mapi(pipeConfig)

        var uri = '/s/'+source_server+'/dbms/tasks/'+h.id
        if(method == "POST") {
          uri += "/input"
        }
        if(method == "output") {
          uri += "/output"
          method = "GET"
        }

        var re = mc.request(method || "GET", uri)
        setupStreamErrorHandler(re, "pipeDbmsRequest", errCallbackOrStr)
        return re

    }

    app.pipeFilemanRequest = function(h, method, errCallbackOrStr) {
        var pipeConfig = app.Mapi.cloneServersAsPipe(app.ServersFileman.FirstServer())
        var mc = app.Mapi(pipeConfig)

        var uri = '/fileman/tasks/'+h.id
        if(method == "POST") {
          uri += "/input"
        }
        if(method == "output") {
          uri += "/output"
          method = "GET"
        }

        var re = mc.request(method || "GET", uri)
        setupStreamErrorHandler(re, "pipeFilemanRequest", errCallbackOrStr)
        return re
    }

    app.getSlowlog=function(){
      const slowlog = require("lib-slowlog.js")(app, app.knex);
      return slowlog;
    }

    const cron = require('Croner');

    var csp = app.config.get("cron_processing")
    if(csp) {
        cron.schedule(csp, function(){
            return Promise.resolve()
              .then(()=>{
                 return statRouter.cronWorks().catch(catcher)
              })
              .then(()=>{
                 return dbsRouter.cronWorks().catch(catcher)
              })
        });

        function catcher(ex){
           console.error("Error: ", ex)
           return Promise.resolve()
        }
    }

    return app


}
