var lib = module.exports = function(app, knex){

    const MError = require("MonsterError")

    const KnexLib = require("MonsterKnex")

    var ValidatorsLib = require("MonsterValidators")

  	var vali = ValidatorsLib.ValidateJs()
    var Common = ValidatorsLib.array()



	var re = {}


  re.PopulatePasswordForUsers = function(users){
     var usernamesByDbms = {};
     users.forEach(user=>{
        var dbms_name = user.dm_dbms_instance_name;
        if(!usernamesByDbms[dbms_name])
            usernamesByDbms[dbms_name] = {};
        usernamesByDbms[dbms_name][user.u_username] = user;
     })

     const dotq = require("MonsterDotq");
     return dotq.linearMap({array: Object.keys(usernamesByDbms), action: function(dbms_name){
        return re.GetDbmsByName(dbms_name)
          .then((dbms)=>{
              return dbms.GetBackend();
          })
          .then(backend=>{
              var usernames = Object.keys(usernamesByDbms[dbms_name]);
              return backend.GetPasswordForUsernames(usernames);
          })
          .then(userPasswordHashes=>{
              // console.log("userPasswordHashes", userPasswordHashes)
              Object.keys(userPasswordHashes).forEach(username=>{
                 usernamesByDbms[dbms_name][username].u_password = userPasswordHashes[username];
              })

              // and everything is fine
          })
     }})
  }


  re.GetDbmsBy = function(filterHash) {

    var f = KnexLib.filterHash(filterHash)

    return knex("dbms").select()
      .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
      .then(rows=>{

              rows.map(row => dbRowToObject(row))
              Common.AddCleanupSupportForArray(rows)

              return Promise.resolve(rows)
      })

  }

  function singleRow(prom) {
     return prom.then(rows=>{
          row = Common.EnsureSingleRowReturned(rows, "DBMS_NOT_FOUND")

          return Promise.resolve(row)
     })
  }

  re.GetDbmsByName = function(name){
    return singleRow(re.GetDbmsBy({dm_dbms_instance_name: name}))
  }

  re.GetDbmsById = function(id){
    return singleRow(re.GetDbmsBy({dm_id: id}))
  }

	re.GetDbms = function(){
		return re.GetDbmsBy({})
	}



	re.Insert = function(in_data, req){

      var d
      return vali.async(in_data, {
         dm_dbms_instance_name: {presence: true, isString: {strictName: true}},
         dm_dbms_type: {presence: true, inclusion: lib.SupportedDbmsTypes()},
         dm_connection_string: {presence: true, isString: true},
       })
      	.then(ad=>{

           d = ad

           return app.getDatabaseBackendLib(d.dm_dbms_type, d.dm_connection_string)

        })
        .then((db)=>{

           return knex
             .insert(d)
             .into("dbms")
             .then(ids=>{
                return Promise.resolve(ids[0])
             })
             .catch(ex=>{
                if(ex.code == 'SQLITE_CONSTRAINT')
                   throw new MError("ALREADY_EXISTS")
                throw ex
             })


        })
        .then((id)=>{
           app.InsertEvent(req, {
		      e_event_type: "dbms-create",
		      e_other: {dm_id:id, dm_dbms_instance_name: in_data.dm_dbms_instance_name}
		   })

           return Promise.resolve(id)
        })

	}

	return re


    function dbRowToObject(dbms){

    	dbms.Cleanup = function(){
    		delete dbms.dm_id
    		return dbms
    	}

      dbms.GetBackend = function(){
           return app.getDatabaseBackendLib(dbms.dm_dbms_type, dbms.dm_connection_string);
      }

      dbms.QueryLiveStatistics = function(){
         return dbms.GetBackend()
           .then(backend=>{
              return backend.QueryLiveStatistics()
           })
      }

      dbms.FlushLogs = function(){
         return dbms.GetBackend()
           .then(backend=>{
              return backend.FlushLogs()
           })

      }

      dbms.StoreStatistics = function(stats){
         var databases = GetDatabaseLibInstance()

         var ps = []
         Object.keys(stats).forEach(databaseName=>{
            var consumptionMb = stats[databaseName]

            ps.push(databases.StoreStatistics(dbms.dm_id, databaseName, app.roundToTwo(consumptionMb)))
         })

         return Promise.all(ps)
      }

      function GetDatabaseLibInstance(){
         const DatabasesLib = require("lib-databases.js")
         var databases = DatabasesLib(app, knex)

         return databases
      }

      dbms.GetDatabases = function(){
         var databases = GetDatabaseLibInstance()
         return databases.GetDatabasesByDbms(dbms.dm_id)
      }

      dbms.Change = function(in_data){
          var d
          return vali.async(in_data, {
             dm_connection_string: {presence: true, isString: true},
           })
          .then(ad=>{

             d = ad

             if(Object.keys(d).length <= 0) throw new MError("NOTHING_TO_UPDATE")

             return app.getDatabaseBackendLib(dbms.dm_dbms_type, d.dm_connection_string)

          })
          .then(()=>{
              if((d.dm_connection_string)&&(d.dm_connection_string != dbms.dm_connection_string))
                 app.removeDatabaseBackendLib(dbms.dm_dbms_type, dbms.dm_connection_string)

              return knex('dbms')
                .whereRaw('dm_id=?', [dbms.dm_id])
                .update(d)

          })
      }

    	dbms.Delete = function(req){

      		return knex("databases").count('db_id AS c')
      		  .whereRaw("db_dbms=?", [dbms.dm_id])
      		  .then(rows=>{
      		  	  if(rows[0].c) throw new MError("NOT_EMPTY")

      	          return knex("dbms")
      	           .whereRaw("dm_id=?", [dbms.dm_id])
      	           .del()

      		  })
            .then(()=>{
               app.InsertEvent(req, {
			     e_event_type: "dbms-remove",
				 e_other: {dm_dbms_instance_name:dbms.dm_dbms_instance_name}
			   })

            })

    	}

    	return dbms

    }

}

lib.SupportedDbmsTypes = function() {
   return ["mysql"]
}
