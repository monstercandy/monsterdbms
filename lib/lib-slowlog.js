/*
   setfacl -m d:g:10000:r /web/mysql
   this will grant read access to the mysql-slow.log file
*/
(function(){


	var ValidatorsLib = require("MonsterValidators");
	var vali = ValidatorsLib.ValidateJs();

  const dotq = require("MonsterDotq");
  const fs = dotq.fs();
  const mysqlSlowlogParserLib = require("lib-mysql-slowlog-parser.js");
  const hourMultiplier = 60*60*1000;


    var lib = module.exports = function(app, knex) {

    const escalate_slowlog_events_interval_hour = app.config.get("escalate_slowlog_events_interval_hour");


    function slowlogQueryParams(d){
          return vali.async(d, {
               "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
               "offset": {default: {value:0}, isInteger:true},
               "order": {inclusion: ["ds_timestamp", "ds_dbms", "ds_db_username","ds_query"], default: "ds_timestamp"},
               "desc": {isBooleanLazy: true},

               // where fields
               "notbefore": {isString: true},
               "notafter": {isString: true},

               "ds_webhosting": {isString:{lazy:true}},
               "ds_dbms": {isString: true},
               "ds_db_username": {isString: true},
               "ds_query": {isString: true},
          })
    }
    function slowlogQueryWhere(d, knex){

       var qStr = [1]
       var qArr = []

       if(d.notbefore){
          qStr.push("ds_timestamp>=?")
          qArr.push(d.notbefore)
       }
       if(d.notafter){
          qStr.push("ds_timestamp<=?")
          qArr.push(d.notafter)
       }

       if(d.ds_webhosting){
          qStr.push("ds_webhosting=?")
          qArr.push(d.ds_webhosting)
       }
       if(d.ds_dbms){
          qStr.push("dbms.dm_dbms_instance_name=?")
          qArr.push(d.ds_dbms)
       }
       if(d.ds_db_username){
          qStr.push("ds_db_username=?")
          qArr.push(d.ds_db_username)
       }
       if(d.ds_query){
          qStr.push("ds_query LIKE ?")
          qArr.push('%'+d.ds_query+'%')
       }

       var qStr = qStr.join(" AND ")

       return knex.whereRaw(qStr, qArr)

    }



    var re = {}


    const moment = require("MonsterMoment") // we require this so now will exist for sure
    const MError = app.MError


    re.Insert = function(in_data){

       //console.log("insert was called", in_data)

       var d;

       return vali.async(in_data, {
       	  ds_dbms: {presence: true, isString: {lazy:true}},
       	  ds_db_username: {presence: true, isString: true},
          ds_database_name: {isString: true},
       	  ds_query: {presence: true, isString: true},
       	  ds_time: {isInteger: true},
       	  ds_locktime: {isInteger: true},
       	  ds_rows_sent: {isInteger: true},
       	  ds_rows_examined: {isInteger: true},
       	  ds_timestamp: {isString: true},
       })
         .then(ad=>{
            d = ad;

            if(d.ds_database_name)
               return lookupDatabase(d)
               .catch(ex=>{
                   if(ex.message == "DATABASE_NOT_FOUND")
                      return;
                   throw ex;
               });
         })
         .then((database)=>{
            if(!database) database = {};
            d.ds_user_id = database.db_user_id || "";
            d.ds_webhosting = database.db_webhosting || 0;

            app.slowlogSync = app.slowlogSync.concat(d);

            app.EscalateEvent("DBMS_SLOWLOG_EVENTS", {slowQuery: d});
         })
    }


    re.Query = function (pd) {
           var d
           return slowlogQueryParams(pd)
              .then(ad=>{
                 d = ad
                 var s = slowlogQueryWhere(d,knex.select("dbmsslow.*", "dbms.dm_dbms_instance_name AS ds_dbms")
                   .table('dbmsslow')
                   .leftJoin("dbms", "dm_id", "ds_dbms")
                   .limit(d.limit)
                   .offset(d.offset)
                   .orderBy(d.order, d.desc?"desc":undefined)
                  )
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {

                return {"events": rows }

              })
    }

    re.Count = function (d) {
           return slowlogQueryParams(d)
              .then(d=>{
                 var s = slowlogQueryWhere(d, knex('dbmsslow').count("*"))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {
                return Promise.resolve({"count": rows[0]["count(*)"] })
              })
    }


    re.setupSlowlogWatchers = function(){
       app.slowlogParsers.forEach(x=>{
        console.log("unwatching parser", x)
          x.parser.unwatch();
       });
       app.slowlogParsers = [];

       var nodes = app.config.get("watch_slowlog");
       if(!nodes) return;
       var dbmsNames = Object.keys(nodes);
       if(!dbmsNames.length) return;

       const dbms = require("lib-dbms.js")(app, knex);

       var timer;
       return dotq.linearMap({
          catch: ex =>{
             console.error("error while configuring slowlog watchers", ex);
             if(timer) return;

             // retrying it in a few minutes
             timer = setTimeout(re.setupSlowlogWatchers, 5*60*1000);
          },
          array: Object.keys(nodes),
          action: dbmsName =>{
             var filename = nodes[dbmsName];

             return dbms.GetDbmsByName(dbmsName)
             .then(dbms=>{

                 if(dbms.dm_dbms_type != "mysql") throw new Error("Unsupported dbms");

                 var parser = mysqlSlowlogParserLib({filename:filename, onEvent: function(event){
                     // console.log("parser event", event)

                     if(!event.ds_database_name)
                       event.ds_query = event.userhostline+"\n"+ event.ds_query;
                     delete event.userhostline;
                     event.ds_dbms = dbms.dm_id;

                     return re.Insert(event);
                 }})

                 app.slowlogParsers.push({filename:filename, parser:parser, dbms: dbms});

             })

          }
       })


    }






    if(!app.slowlogSync) app.slowlogSync = [];

    if(!app.slowlogTimer)
        rescheduleSlowlogSyncTimer();

    if(!app.slowlogParsers) {
       app.slowlogParsers = [];
       re.setupSlowlogWatchers();
    }

    if(!app.slowlogRotate)
      setupSlowlogRotator();



    return re;

    function setupSlowlogRotator(){
       setInterval(function(){
           if(!app.slowlogParsers.length) return;

           return dotq.linearMap({array:app.slowlogParsers, catch: true, action: stuff=>{
              console.log("doing slowlog rotation", stuff.filename);
              return fs.unlinkAsync(stuff.filename)
                .then(()=>{
                   return stuff.dbms.FlushLogs();
                })

           }})

       }, app.config.get("rotate_slowlog_after_hours")*hourMultiplier);
    }


    function rescheduleSlowlogSyncTimer(){
        app.eventSyncTimer = setTimeout(function(){

            var dataToInsert = app.slowlogSync;
            app.slowlogSync = [];

            return Promise.resolve()
              .then(()=>{
                 if(!dataToInsert.length) return;

                  return knex.transaction(trx=>{

                     return dotq.linearMap({array:dataToInsert,action:function(row){
                        return trx.into("dbmsslow").insert(row).return()
                     }})

                  })
                  .then(()=>{
                     console.log("Successfully inserted", dataToInsert.length, "slowlog events");
                  })


              })
              .catch(ex=>{
                 // but the show must go on...
                 console.error("We were unable to flush events into slowlog", ex)
              })
              .then(()=>{
                 return rescheduleSlowlogSyncTimer()
              })


        }, app.config.get("slowlog_sync_flush_seconds")*1000)

    }

    function lookupDatabase(d) {
       const dbs = require("lib-databases.js")(app, knex);
       return dbs.GetDatabaseByDbmsAndName(d.ds_dbms, d.ds_database_name);
    }

}

})()


