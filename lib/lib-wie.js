var lib = module.exports = function(app, knex) {

   const MError = app.MError;

   const dbmsLib = require("lib-dbms.js")
   var dbms = dbmsLib(app, knex);

   const databasesLib = require("lib-databases.js")
   var databases = databasesLib(app, knex);

   const dbusersLib = require("lib-dbusers.js")

   var wie = {};

   const dotq = require("MonsterDotq");
   const moment = require("MonsterMoment");

   wie.BackupWh = function(wh, req) {

      var allUserGroups = [];
      var allUsers;

      var re = {};
      return databases.GetDatabasesByWebhosting(wh.wh_id)
        .then(dbs=>{
           re.databases = [];


           return dotq.linearMap({array: dbs, action: function(db){

              var dbLine = {props: db};
              re.databases.push(dbLine);


              var dbusers = dbusersLib(app, knex, db);
              return dbusers.GetUsers()
                .then(users=>{

                    dbLine.users = users;
                    allUserGroups.push(users);

                    db.db_dbms = db.dm_dbms_instance_name;
                    Array("db_id", "db_dbms_type", "db_user_id", "db_webhosting", "dm_id", "dm_connection_string", "dm_dbms_type", "dm_dbms_instance_name").forEach(x=>{
                        delete db[x];
                    })



                })

           }})


        })
        .then(()=>{

           allUsers = [].concat.apply([], allUserGroups);

           return dbms.PopulatePasswordForUsers(allUsers);

        })
        .then(()=>{

           const legacyHosts = app.config.get("wie_legacy_user_hosts");

           allUsers.forEach(user=>{

              Array("db_dbms", "db_id", "db_database_name", "dm_dbms_instance_name", "dm_dbms_type", "db_locked", "db_tally_mb", "db_user_id", "db_webhosting", "dm_connection_string", "dm_id", "u_database", "u_dbms", "u_id", "u_user_id", "u_webhosting").forEach(x=>{
                  delete user[x];
              });

              if(legacyHosts.indexOf(user.u_hostname) > -1) {
                 user.u_hostname = "";
              }
           })

           return re;
        })
   }

   // wrapping the restore all operation in a huge transaction
   wie.RestoreAllWhs = function(whs, in_data, req){
      return knex.transaction(function(trx){
          var wieTrx = require("Wie").transformWie(app, lib(app, trx));
          return wieTrx.GenericRestoreAllWhs(whs, in_data, req);
      })
   }


   wie.GetAllWebhostingIds = function(){
      return databases.GetWebhostingIds()
        .then(rows=>{
            return rows.map(x => x.db_webhosting)
        })
   }

   wie.RestoreWh = function(wh, in_data, req) {

      // lets put the site first
      return dotq.linearMap({array: in_data.databases, action: in_data=>{

         var db = in_data.props;

         var now = moment.now();

         db.db_webhosting = wh.wh_id;
         db.db_user_id = wh.wh_user_id;

         db.updated_at = now;

         return databases.Insert(db, req)
           .then((id)=>{
              return databases.GetDatabaseById(id);
           })
           .catch(ex=>{
              if((ex.message != "ALREADY_EXISTS") || (!ex.underlyingException) || (!ex.underlyingException.conflictingDatabase))
                throw ex;

              // otherwise its might be fine:
              if(ex.underlyingException.conflictingDatabase.db_webhosting != wh.wh_id)
                  throw new MError("DATABASE_WITH_SAME_NAME_ALREADY_ATTACHED_TO_OTHER_STORAGE");

              return ex.underlyingException.conflictingDatabase;
           })
           .then(database=>{

              var dbusers = dbusersLib(app, knex, database);
              return dotq.linearMap({array: in_data.users, action: function(user){
                  user.u_webhosting = wh.wh_id;
                  user.u_user_id = wh.wh_user_id;
                  user.updated_at = now;

                  return dbusers.Insert(user, req)
                     .catch(ex=>{
                        if((ex.message != "ALREADY_EXISTS") || (!ex.underlyingException) || (!ex.underlyingException.conflictingUser))
                          throw ex;

                        // otherwise its might be fine:
                        if(ex.underlyingException.conflictingUser.u_webhosting != wh.wh_id)
                            throw new MError("DATABASE_USER_WITH_SAME_NAME_ALREADY_ATTACHED_TO_OTHER_DATABASE");
                     });

              }})
           })

      }})
      .then(()=>{
          app.InsertEvent(req, {e_event_type: "restore-wh-databases", e_other: wh.wh_id})

      })



   }



   return wie;

}
