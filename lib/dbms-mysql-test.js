require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var mysql
const MysqlLib = require("./lib-mysql.js")
MysqlLib(JSON.stringify({host:"localhost",user:"root",password:""}))
  .then(amysql=>{
  	  mysql = amysql
  	  console.log("we got the mysql object")
  	  return mysql.Test()
  })
  .then(()=>{
  	 console.log("connection tested")
  	 return mysql.CreateDatabase("foobar")
  })
  .then(()=>{
  	 console.log("database created")
  	 return mysql.CreateUser("foobar", "username", "password")
  })
  .then(()=>{
  	 console.log("user created")
  	 return mysql.DeleteUser("username")
  })
  .then(()=>{
  	 console.log("user deleted")
  	 return mysql.DeleteDatabase("foobar")
  })
  .then(()=>{
  	 console.log("database deleted")

     return mysql.QueryLiveStatistics()
  })
  .then((stats)=>{
     console.log("stats", stats)
  })
  .catch(ex=>{
  	console.error("error:", ex)
  })