var ValidatorsLib = require("MonsterValidators")
var vali = ValidatorsLib.ValidateJs()

var lib = module.exports = function(connectionString, app) {

	if(!app) app = {config: {get: function(){}}}

	return Promise.resolve()
	  .then(()=>{
          var connectionParams = JSON.parse(connectionString)

          return vali.async(connectionParams, {
					host:{presence:true,isString:true},
					user:{presence:true,isString:true},
					password:{isString:true},
				})
	  })
	  .then(connectionParams=>{

	  	  var knex = require("MonsterKnex").Knex({"client":"mysql", connection:connectionParams})

	  	  var re = {}

	  	  re.Test = function(){
            return sendOk(
            	knex.raw('SELECT NOW() AS c').then((n)=>{
            	  console.log("current time by dbms", n)
                })
            )
	  	  }


	  	  re.GetPasswordForUsernames = function(usernames){
             return knex("mysql.user").select("User","Password").whereIn("mysql.user.User", usernames)
               .then(rows=>{
           	   	   var re = {};
           	   	   rows.forEach(row=>{
           	   	   	  re[row.User] = row.Password;
           	   	   })
           	   	   return re;
               })

	  	  }


	  	  re.GetScoreboard = function(){
             return knex.raw("SHOW PROCESSLIST")
               .then(rows=>{
           	   	   return Promise.resolve(rows[0])
               })

	  	  }

	  	  re.FlushLogs = function(){
	  	  	 return knex.raw("FLUSH LOGS").return();
	  	  }

	  	  re.RepairAllDatabases = function(params){
	  	  	 var cmd = app.config.get("cmd_mysql_repair_all")
	  	  	 var backendOptions = app.config.get("mysql")

             return app.commander.spawn({chain:cmd}, [connectionParams, backendOptions])
	  	  }
	  	  re.Repair = function(database, params){
	  	  	 return vali.async(params||{}, {table_name:{isString:true}})
	  	  	   .then(d=>{
			  	  	 var cmd = app.config.get(d.table_name ? "cmd_mysql_repair_table" : "cmd_mysql_repair_db")
			  	  	 var backendOptions = app.config.get("mysql")

		             return app.commander.spawn({chain:cmd}, [connectionParams, database, backendOptions, d])
	  	  	   })
	  	  }

	  	  re.Import = function(database, tmpUser, params){
	  	  	 var other = {gunzip: ""}
	  	  	 if((params.filename)&&(params.filename.match(/\.(gz|GZ)$/)))
	  	  	 	other.gunzip = "gunzip"
	  	  	 var cmd = app.config.get("cmd_mysql_import")
	  	  	 var backendOptions = app.config.get("mysql")

	  	  	 // note: removeImmediately: false, this is needed so that the clients would be able to see the response even for fileman based imports
             return app.commander.spawn({chain:cmd, upload: true}, [connectionParams, database, tmpUser, backendOptions, other])

	  	  }

	  	  re.Export = function(database, params){
	  	  	 var cmd = app.config.get("cmd_mysql_export")
	  	  	 const moment = require("MonsterMoment")
	  	  	 var nowStr = moment.nowRaw()
	  	  	 var backendOptions = app.config.get("mysql")
             return app.commander.spawn(
             	{chain:cmd, download: true, downloadMimeType: "application/gzip", downloadAs: database.db_database_name+"-"+nowStr+".sql.gz"},
             	[connectionParams, database, backendOptions]
             )
	  	  }

	  	  re.QueryLiveStatistics = function() {
/*
Will return something like this:
+--------------+--------------+
| DataBaseName | DataBaseSize |
+--------------+--------------+
| 369hu        |   3.53876209 |
+--------------+--------------+
1 row in set (2.65 sec)
*/

             var re = {}
             return knex.raw("SELECT table_schema 'DataBaseName', (SUM( data_length + index_length ) / 1024 / 1024) 'DataBaseSizeMb' FROM information_schema.TABLES GROUP BY table_schema")
               .then(rows=>{
           	   	   rows[0].forEach(row=>{
           	   	   	  re[row.DataBaseName] = row.DataBaseSizeMb
           	   	   })

               	   return Promise.resolve(re)
               })
	  	  }

	  	  re.CreateDatabase = function(dbName){
	  	  	// dbName was already verified in the upper layers

	  	  	var sql = 'CREATE DATABASE `'+dbName+'`'
	  	  	var defCharset = app.config.get("default_charset")
	  	  	if(defCharset) {
	  	  		sql += ` DEFAULT CHARSET ${defCharset}`
	  	  		var defCollate = app.config.get("default_collate")
	  	  		if(defCollate)
	  	  			sql += ` DEFAULT COLLATE ${defCollate}`
	  	  	}

	  	  	return sendOk(knex.raw(sql))
	  	  }
	  	  re.DeleteDatabase = function(dbName){
	  	  	return sendOk(knex.raw('DROP DATABASE `'+dbName+'`'));
	  	  }
	  	  re.CreateUser = function(dbName, username, password, hostname){
	  	  	var identifyStr = "BY ?";
	  	  	if(password.match(/^\*[A-Z0-9]+$/))
	  	  		identifyStr = "WITH mysql_native_password AS ?";
	  	  	var q = knex.raw("GRANT ALL PRIVILEGES ON `"+dbName+"`.* TO ?@? IDENTIFIED "+identifyStr, [username, hostname || "%", password])
	  	  	// console.log(q.toString())
	  	  	return sendOk(q);
	  	  }
	  	  re.DeleteUser = function(username, hostname){
	  	  	return sendOk(knex.raw("DROP USER ?@?", [username, hostname || "%"]));
	  	  }
	  	  re.UserToReadOnly = function(dbName,username,hostname) {
            return sendOk(knex.raw("REVOKE update, insert ON `"+dbName+"`.* FROM ?@?", [username, hostname||"%"]));
	  	  }
	  	  re.UserToReadWrite = function(dbName,username,hostname) {
            return sendOk(knex.raw("GRANT update, insert ON `"+dbName+"`.* TO ?@?", [username, hostname||"%"]));
	  	  }

	  	  re.Teardown = function() {
	  	  	 return knex.destroy()
	  	  }

	  	  return Promise.resolve(re)

	  	  function sendOk(prom){
	  	  	 return prom.then(()=>{
	  	  	 	return Promise.resolve("ok")
	  	  	 })
	  	  }

	  })


}

/*
it("mysql test", function(done){

   var knex = require("MonsterKnex").Knex({"client":"mysql", connection:{
     host:"127.0.0.1",
     user:"root",
     password:"",
    }})

    return knex.raw('select NOW() AS c')
     .then((x)=>{
      console.log(x[0][0].c)
        return Promise.resolve("ok")
     })

})
*/

